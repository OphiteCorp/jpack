# jPack

This application is used to encrypt the entire directory including some data compression.\
It should be noted that the directory to be encrypted is read-only. The application will create a single pack file that will contain the entire directory.\
The output pack file can also be protected by a secret AES 256bit key.

## Requirements
Java 11+

## Download
1) Go to the **Tags** section.
2) Find the latest version of the app.
3) Click the Download button -> Download artifacts -> package-release (it is a zip file, then unzip it).

## Actions / Mods

1) **pack** - Creates a single pack file from the directory.\
The principle is that the application scans the entire directory, including all subdirectories, and creates a single file. This file can have different settings from compression, encrypt and more. We will describe below.
2) **unpack** - Extracts the created pack file into the target directory with the complete directory structure with which it was created.
3) **info** - Provides information about the pack file without unpacking, including listing the entire directory structure inside the file.

## Commands / Parameters

`-m` = (**required**) Specifies actions (called mode)
> **Examples:**\
> `-m:pack`, `-m:unpack`, `-m:info`

`-in` = (**required**) Input. To create a pack file, it's a directory. When you unpack a pack file or get information, it will be the path to the pack file.
> **Examples:**\
> `-in:"e:/data/private photos"` (this is the directory for the pack action)\
> `-in:"e:/backup/private photos.jp"` (it is a pack file for unpacking or info)

`-out` = Output. When you create a pack file, it is the directory from which to create the file. In case of unpacking
 or getting information, there will be a path to the pack file. If the parameter is not specified, the current directory is used (only for unpack).
> **Examples:**\
> `-out:"e:/backup/private photos.jp"` (created pack file for some directory)\
> `-out:"e:/data/private photos"` (output directory to unpack the pack file)

`-key` = It will be used to create a pack file. If not specified, random is generated. It is also required when working with a pack file that is locked with a key.
> **Examples:**\
> `-key:f1025b5c12416ead626decb294cac78d5f0d9612aa8aed4be2f9ab677d36ecbc` Default is: null\
> if it is not used, a random one is generated and then printed to the console.

`-mt` = Uses multithreaded processing. I recommend always leave it on.
> **Examples:**\
> `-mt:yes` or `-mt:no` Default is: Yes\
> I recommend always leave it on. The more logical CPUs you have, the faster it will be.

`-c` = The mode of compression pack file. Supported are: NONE, LZ4, LZ4HC, GZIP, BZIP2, XZ.
> **Examples:**\
> `-c:lz4`, `-c:bzip2` Default is: GZIP\
> Generally I recommend GZIP, BZIP2, or LZ4. There is no point not to use. The fastest LZ4 is almost comparable to no compression.\
> I have to mention the disadvantage of LZ4 and LZ4HC that the output is partially readable by the user (unless another layer of protection is used).

`-cl` = Compression level. The level can only be set for GZIP, XZ types where is range [0-9] and for LZ4HC is [1-17 (9 is the best)].
> **Examples:**\
> `-cl:9` Default is: 1\
> It is not always worth setting the highest compression. The time it takes to create a pack file can be drastic. Especially for XZ or high levels of LZ4HC. It also depends on how big the directory is.

`-e` = The mode of data encryption in the pack file. Supports ALL and HEADER only. It uses the best AES-GCM 256bit, which is also used in government and CIA.
> **Examples:**\
> `-e:all` or `-e:header` Default is: all\
> I recommend encrypt all, performance degradation is not so severe. When encrypting all, at least some data compression is recommended.

`-r` = Apply rot128 to data buffers. This is an additional layer of security to prevent the user from reading the file.
> **Examples:**\
> `-r:yes` or `-r:no` Default is: Yes\
> It has low performance impacts. It can still be turned on.

`-s` = Apply shuffle to data buffers. This is an additional layer of security to prevent the user from reading the file. Significantly better than rot128.
> **Examples:**\
> `-s:yes` or `-s:no` Default is: No\
> The principle is to randomly reorder and change bytes in buffers. This will prevent the readability of the data. Each file has its own key, by which it is possible to re-order the data to its original state.\
> It has a big impact on performance (about 3 times slower according to the buffer size), even on unpacking a pack file.

`-pass` = The pack file will require a secret key to unpack or info. Without knowing the secret key, no one, nor the government will be able to open the pack.
> **Examples:**\
> `-pass:yes` or `-pass:no` Default is: No\
> If the password is not enabled, everyone can use this application to unpack the pack file. However, if you use a password and you lose it, it is impossible to recover the data!

`-bs` = Buffer size to create pack file. The size may vary depending on the type of jPack settings and file types in the directory.
> **Examples:**\
> `-bs:2097152` Default is: 131072\
> The value is in bytes. A higher value means better compression but longer processing. Too low value, very low compression and also long processing.\
> The ideal range is between 8KB (1024 * 8) and 2MB (1024 * 1024 * 2) depending on the application settings.

`-purge` = Deletes the source. If you create a pack file, it will delete the entire directory. If you unpack a pack file, it will delete the pack file itself.
> **Examples:**\
> `-purge:yes` or `-purge:no` Default is: No

`-detail` = Displays the contents of the pack file. Lists the directory tree of a saved pack file.
> **Examples:**\
> just `-detail`\
> For INFO only.

`-silent` = This mode does not write anything to the console. It simply creates or unpacks the pack file in the background.
> **Examples:**\
> just `-silent`\
> I do not recommend using it with the -pass parameter. This will lose access to the pack file if you did not use your own key.

`--help` = Displays help.

## Examples

Only create a pack file without any protection or compression (creates a file from the directory).\
`java -jar jpack.jar -m:pack -in:"E:\_temp\testdir" -out:"E:\_temp\data.jp" -e:header -c:none -r:no`

Maximum data protection. The compression type is arbitrary (i recommend gzip / bzip2).\
`java -jar jpack.jar -m:pack -in:"E:\_temp\testdir" -out:"E:\_temp\data.jp" -c:bzip2 -s:yes -pass:yes`

Creates a pack file from the directory and provides password protection.\
`java -jar jpack.jar -m:pack -in:"E:\_temp\testdir" -out:"E:\_temp\data.jp" -pass:yes`\
The generated key is: f706d1b91a6f8f30610086af9fab416f29a2de5b38c79db025f779e84ab01285

**A dangerous combination.** Creates a pack file with the generated key, but in `-silent` mode where the key will not be
 displayed in the console.\
You can also use the `-purge` parameter to delete the original directory. Other parameters just speed up the process.\
`java -jar jpack.jar -m:pack -in:"E:\_temp\testdir" -out:"E:\_temp\data.jp" -c:lz4 -r:no -pass:yes -purge -silent`

Unpack (restore) the pack file back to the directory.\
`java -jar jpack.jar -m:unpack -in:"E:\_temp\data.jp" -out:"E:\_temp\mydata" -key:f706d1b91a6f8f30610086af9fab416f29a2de5b38c79db025f779e84ab01285`\
The `-key` parameter is not necessary, if it is not filled in, the application will request the password itself.

Unpack the pack file in the background and deletes the pack file.\
`java -jar jpack.jar -m:unpack -in:"E:\_temp\data.jp" -out:"E:\_temp\mydata" -silent -purge`

Unpack the pack file to the current directory (e:\\_temp\\data).\
`java -jar jpack.jar -m:unpack -in:"E:\_temp\data.jp"`

Displays pack file information.\
`java -jar jpack.jar -m:info -in:"E:\_temp\data.jp" -key:f706d1b91a6f8f30610086af9fab416f29a2de5b38c79db025f779e84ab01285`\
Again, the `-key` parameter is not required.

## Demos from the console

### Pack
```
          _  ____                __
         (_)/ __ \ ____ _ _____ / /__
        / // /_/ // __ `// ___// //_/
       / // ____// /_/ // /__ / ,<
    __/ //_/     \__,_/ \___//_/|_|
   /___/

 +
 | jPack - archiver/encryptor for sensitive data
 | by mimic | (c) 2020
 +----------------------------------------------------------------------------------+
 | Create a pack file from the directory
 |
 | Directory: E:\_temp\testdir
 +
[X] 100,00% | 13/13 [==================================================] Scanning successed
 +
 | Total files: 13
 | Total directories: 3
 | Directory size: 75,58 MB
 |
 | Version: 1.0.0
 | Compress mode: BZIP2
 | Encrypt mode (AES-GCM 256bit): ALL
 | Buffer size: 131072 (128 KB)
 | Use rot128: True
 | Use shuffle: True
 | Use passwordless: False
 | Use multithreaded: True (16 threads)
 +
[X] 100,00% | 13/13 [==================================================] Packing successed                                       
 +
 | Pack: E:\_temp\data.jp
 | Pack size: 3,93 MB from 75,58 MB directory
 | Created: 23:46:47 - 29.12.2019
 | Packing time: 00:00:03.540
 +----------------------------------------------------------------------------------+
 | Secret key: 5683b2163f2956ca78eafc747d5ed6b94a2ec065db4c327162c48f06069bc423
 +----------------------------------------------------------------------------------+
 | Please keep this secret key. The application uses the strongest security.
 | Without the key, you will never be able to recover your data!
 | I am not responsible for data loss. Thank you for understanding.
 +
```
### Unpack
```
         _  ____                __
         (_)/ __ \ ____ _ _____ / /__
        / // /_/ // __ `// ___// //_/
       / // ____// /_/ // /__ / ,<
    __/ //_/     \__,_/ \___//_/|_|
   /___/

 +
 | jPack - archiver/encryptor for sensitive data
 | by mimic | (c) 2020
 +----------------------------------------------------------------------------------+
 | Unpack the pack file to a directory
 |
 | Pack: E:\_temp\data.jp
 | Pack size: 3,93 MB
 | Output directory: E:\_temp\mydata
 +
 | Total files: 13
 | Total directories: 3
 | Directory size: 75,58 MB
 |
 | Version: 1.0.0
 | Compress mode: BZIP2
 | Encrypt mode (AES-GCM 256bit): ALL
 | Buffer size: 131072 (128 KB)
 | Use rot128: True
 | Use shuffle: True
 | Use passwordless: False
 | Created: 23:46:47 - 29.12.2019
 | Packing time: 00:00:03.540
 | Used threads: 16
 +
[X] 100,00% | 13/13 [==================================================] Unpacking successed                                     
 +
 | Unpack time: 00:00:01.969
 +
 ```
 ### Info (with detail => which adds a view of the directory tree)
 ```
           _  ____                __
         (_)/ __ \ ____ _ _____ / /__
        / // /_/ // __ `// ___// //_/
       / // ____// /_/ // /__ / ,<
    __/ //_/     \__,_/ \___//_/|_|
   /___/

 +
 | jPack - archiver/encryptor for sensitive data
 | by mimic | (c) 2020
 +----------------------------------------------------------------------------------+
 | Get pack file information
 |
 | Pack: E:\_temp\data.jp
 | Pack size: 3,93 MB
 +
[X] 100,00% | 1/1 [==================================================] Opening successed
 +
 | Total files: 13
 | Total directories: 3
 | Directory size: 75,58 MB
 |
 | Version: 1.0.0
 | Compress mode: BZIP2
 | Encrypt mode (AES-GCM 256bit): ALL
 | Buffer size: 131072 (128 KB)
 | Use rot128: True
 | Use shuffle: True
 | Use passwordless: False
 | Created: 23:46:47 - 29.12.2019
 | Packing time: 00:00:03.540
 | Used threads: 16
 +----------------------------------------------------------------------------------+
 | Attention! This pack file requires a secret key to unlock.
 +----------------------------------------------------------------------------------+
 | Directory in pack file:
 |
  `-- [pack]
      |-- [init.d]
      |   |-- PSOCache.bin (75,15 MB)
      |   `-- readme.txt (99 B)
      |-- [security]
      |   |-- [custom]
      |   |   |-- README.md (455 B)
      |   |   |-- gmock-generated-actions.h (375 B)
      |   |   `-- gmock-port.h (1,89 KB)
      |   |-- blacklisted.certs (1,24 KB)
      |   |-- cacerts (96,01 KB)
      |   |-- default.policy (9,99 KB)
      |   `-- public_suffix_list.dat (230,28 KB)
      `-- Uninstall.exe (95,39 KB)
 +----------------------------------------------------------------------------------+
 ```
