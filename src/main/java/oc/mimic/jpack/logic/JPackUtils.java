package oc.mimic.jpack.logic;

import net.jpountz.lz4.LZ4Factory;
import oc.mimic.jpack.component.AesGcmCipher;
import oc.mimic.jpack.helper.IOBytesHelper;
import oc.mimic.jpack.logic.exception.InvalidSecretKeyException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

/**
 * Pomocné metody pro práci s JPack.
 *
 * @author mimic
 */
public final class JPackUtils {

  /**
   * Instance pro LZ4 factory.
   */
  public static final LZ4Factory LZ4_FACTORY = LZ4Factory.fastestInstance();
  public static final SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss - dd.MM.yyyy");

  /**
   * Validuje tajný klíč.
   *
   * @param secretKey Tajný klíč.
   */
  public static void checkSecretKey(String secretKey) {
    if (secretKey == null) {
      throw new InvalidSecretKeyException("The input secret key is empty.");
    }
    var bytes = secretKey.getBytes(StandardCharsets.UTF_8);
    if (bytes.length % AesGcmCipher.AES_SECRET_KEY_LENGTH != 0 ||
        bytes.length > AesGcmCipher.AES_SECRET_KEY_LENGTH * 2) {
      throw new InvalidSecretKeyException("The input secret is not valid.");
    }
  }

  /**
   * Načte číselnou hodnotu z random access file. Ponechá vstupní pointer v RAF.
   *
   * @param raf     Instance random access file.
   * @param reverse Byl na číselnou hodnotu použit reverse? (poslední byte je flag, normálně je první)
   *
   * @return Pár, kde klíš je celkový počet bytů čísla a hodnotou číslo.
   */
  public static NumberData readNumber(RandomAccessFile raf, boolean reverse) throws IOException {
    var currentPos = raf.getFilePointer();
    var flagCode = raf.readByte();
    var flagCodeLength = IOBytesHelper.getNumberFlagLength(flagCode);
    var buffer = new byte[flagCodeLength + 1];

    if (reverse) {
      raf.seek(currentPos - flagCodeLength);
      raf.read(buffer);
      IOBytesHelper.reverse(buffer);
    } else {
      raf.seek(currentPos);
      raf.read(buffer);
    }
    var number = IOBytesHelper.fromNumberBytes(buffer);
    raf.seek(currentPos);
    return new NumberData((byte) (flagCodeLength + 1), number);
  }

  /**
   * Naformátuje čas v milisekundách.
   *
   * @param time Vstupní čas.
   *
   * @return Čitelný formát času.
   */
  public static String formatMsTime(long time) {
    var h = TimeUnit.MILLISECONDS.toHours(time);
    var m = TimeUnit.MILLISECONDS.toMinutes(time - TimeUnit.HOURS.toMillis(h));
    var s = TimeUnit.MILLISECONDS.toSeconds(time - TimeUnit.HOURS.toMillis(h) - TimeUnit.MINUTES.toMillis(m));
    var ms = TimeUnit.MILLISECONDS
            .toMillis(time - TimeUnit.HOURS.toMillis(h) - TimeUnit.MINUTES.toMillis(m) - TimeUnit.SECONDS.toMillis(s));
    return String.format("%02d:%02d:%02d.%03d", h, m, s, ms);
  }

  /**
   * Převede verzi na řetězec.
   *
   * @param version Verze.
   *
   * @return Verze jako řetězec.
   */
  public static String versionToStr(short version) {
    var arr = String.valueOf(version).toCharArray();
    var str = new StringBuilder();

    for (var i = 0; i < arr.length; i++) {
      str.append(arr[i]);

      if (i < arr.length - 1) {
        str.append(".");
      }
    }
    return str.toString();
  }

  /**
   * Převede celý stacktrace výjimky do stringu.
   *
   * @param ex Výjimka.
   *
   * @return Stacktrace.
   */
  public static String stackTraceToString(Exception ex) {
    if (ex != null) {
      try (var sw = new StringWriter()) {
        try (var pw = new PrintWriter(sw)) {
          ex.printStackTrace(pw);
        }
        return sw.toString();
      } catch (Exception e) {
        // nic
      }
    }
    return "";
  }

  /**
   * Informace o načteném číslu.
   */
  public final static class NumberData {

    private final byte numberLength;
    private final Number number;

    /**
     * Vytvoří instanci čísla.
     *
     * @param numberLength Délka čísla v bytech dle datového typu.
     * @param number       Číslo.
     */
    public NumberData(byte numberLength, Number number) {
      this.numberLength = numberLength;
      this.number = number;
    }

    public byte getNumberLength() {
      return numberLength;
    }

    public Number getNumber() {
      return number;
    }
  }
}
