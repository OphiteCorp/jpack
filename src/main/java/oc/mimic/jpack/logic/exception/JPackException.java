package oc.mimic.jpack.logic.exception;

/**
 * Hlavní výjimka aplikace.
 *
 * @author mimic
 */
public class JPackException extends RuntimeException {

  private static final long serialVersionUID = -2266637279230143333L;

  public JPackException(String message) {
    super(message);
  }

  public JPackException(String message, Throwable cause) {
    super(message, cause);
  }
}
