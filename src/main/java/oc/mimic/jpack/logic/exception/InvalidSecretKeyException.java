package oc.mimic.jpack.logic.exception;

/**
 * Neplatný tajný klíč.
 *
 * @author mimic
 */
public final class InvalidSecretKeyException extends JPackException {

  private static final long serialVersionUID = 8512372900774590882L;

  public InvalidSecretKeyException(String message) {
    super(message);
  }

  public InvalidSecretKeyException(String message, Throwable cause) {
    super(message, cause);
  }
}
