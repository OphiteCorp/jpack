package oc.mimic.jpack.logic.exception;

/**
 * Nelze otevřít hlavičku v pack souboru.
 *
 * @author mimic
 */
public final class InvalidOpenPackHeaderException extends JPackException {

  private static final long serialVersionUID = -3210351613763496203L;

  public InvalidOpenPackHeaderException(String message) {
    super(message);
  }

  public InvalidOpenPackHeaderException(String message, Throwable cause) {
    super(message, cause);
  }
}
