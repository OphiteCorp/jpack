package oc.mimic.jpack.logic;

import oc.mimic.jpack.logic.listener.InfoListener;
import oc.mimic.jpack.logic.listener.PackListener;
import oc.mimic.jpack.logic.listener.UnpackListener;
import oc.mimic.jpack.struct.*;

/**
 * Rozhraní pro pack/unpack adresáře.
 *
 * @author mimic
 */
public interface JPackService {

  /**
   * Verze aplikace. Definuje i verzi packu pro případné aktualizace.
   */
  short VERSION = 1_0_0;

  /**
   * Maximální počet vláken, které je možné použít.
   */
  int MAXIMUM_THREADS = Runtime.getRuntime().availableProcessors();

  /**
   * Magický byty pro identifikaci packu.
   */
  byte[] MAGIC_BYTES = new byte[]{ '\0', 'J', 'P' };

  /**
   * Vytvoří novou instanci služby pro pack/unpack.
   *
   * @return Nová instance služby.
   */
  static JPackService createInstance() {
    return new JPackServiceImpl();
  }

  /**
   * Vytvoří novou instanci služby pro pack/unpack.
   *
   * @param packListener   Listener pro odchycení událostí pro pack souboru.
   * @param unpackListener Listener pro odchycení událostí pro unpack pack souboru.
   * @param infoListener   Listener pro odchycení událostí pro info pack souboru.
   *
   * @return Nová instance služby.
   */
  static JPackService createInstance(PackListener packListener, UnpackListener unpackListener,
          InfoListener infoListener) {

    return new JPackServiceImpl(packListener, unpackListener, infoListener);
  }

  /**
   * Vytvoří nový pack soubor z adresáře.
   *
   * @param config         Konfigurace pro vytvoření pack souboru.
   * @param inputDirectory Vstupní adresář, ze kterého se má vytvořit pack soubor.
   * @param outputPackFile Výstupní pack soubor.
   *
   * @return Informace o vytvořeném pack souboru.
   */
  Pack pack(PackConfig config, String inputDirectory, String outputPackFile);

  /**
   * @param config          Konfigurace pro rozbalení pack souboru.
   * @param inputPackFile   Vstupní pack soubor.
   * @param outputDirectory Výstupní adresář, kam se má pack soubor rozbalit.
   *
   * @return Informace o výsledku.
   */
  Unpack unpack(UnpackConfig config, String inputPackFile, String outputDirectory);

  /**
   * Přečte hlavičku pack souboru.
   *
   * @param inputPackFile Vstupní pack soubor.
   * @param secretKey     Tajný klíč. Je potřeba v případě, že se nejedná o pack bez hesla.
   *
   * @return Hlavička pack souboru.
   */
  PackHeaderFull readHeader(String inputPackFile, String secretKey);

  /**
   * Přečte hlavičku pack souboru.
   *
   * @param inputPackFile Vstupní pack soubor.
   *
   * @return Hlavička pack souboru.
   */
  default PackHeaderFull readHeader(String inputPackFile) {
    return readHeader(inputPackFile, null);
  }
}
