package oc.mimic.jpack.logic.handler;

import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.component.ProgressBar;
import oc.mimic.jpack.logic.JPackPrinter;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

import java.util.List;
import java.util.Scanner;

/**
 * Handler událostí vytvoření packu pro konzoli.
 *
 * @author mimic
 */
abstract class AbstractJPackConsoleHandler {

  protected final ProgressBar bar = new ProgressBar();

  protected String waitForSecretKey() {
    try (var scanner = new Scanner(System.in)) {
      _lineWithoutNl("Secret key: ");
      return scanner.next();
    }
  }

  protected final void _plus() {
    System.out.println(" +");
  }

  protected final void _line(String format, Object... args) {
    System.out.println(" | " + String.format(format, args));
  }

  protected final void _lineWithoutNl(String format, Object... args) {
    System.out.print(" | " + String.format(format, args));
  }

  protected final void _line() {
    System.out.println(" | ");
  }

  protected final void _separator() {
    System.out.println(" +----------------------------------------------------------------------------------+");
  }

  protected final void printTitle(List<String> titleLines) {
    _plus();
    _line("jPack - archiver/encryptor for sensitive data");
    _line("by mimic | (c) 2020");
    _separator();

    for (var line : titleLines) {
      _line(line);
    }
    _plus();
  }

  protected final void printTotalFiles(Number totalFiles) {
    _line("Total files: %s", totalFiles);
  }

  protected final void printTotalDirectories(Number totalDirectories) {
    _line("Total directories: %s", totalDirectories);
  }

  protected final void printDirectorySize(long directorySize) {
    _line("Directory size: %s", FilesizeFormatter.format(directorySize));
  }

  protected final void printCompressMode(CompressMode compressMode, Integer compressLevel) {
    var level = "";

    if (compressMode.isHasLevel() && compressLevel != null) {
      level = " (level: " + compressLevel + " from " + compressMode.getMaxLevel() + ")";
    }
    _line("Compress mode: %s", compressMode + level);
  }

  protected final void printEncryptMode(EncryptMode encryptMode) {
    _line("Encrypt mode (AES-GCM 256bit): %s", encryptMode);
  }

  protected final void printBufferSize(int bufferSize) {
    _line("Buffer size: %s", JPackPrinter.toBufferSizeStr(bufferSize));
  }

  protected final void printRot128(boolean value) {
    _line("Use rot128: %s", JPackPrinter.toStr(value));
  }

  protected final void printShuffle(boolean value) {
    _line("Use shuffle: %s", JPackPrinter.toStr(value));
  }

  protected final void printPasswordless(boolean value) {
    _line("Use passwordless: %s", JPackPrinter.toStr(value));
  }

  protected final void printMultithreaded(boolean value) {
    _line("Use multithreaded: %s", JPackPrinter.toMultithreadedStr(value));
  }

  protected final void printPack(String packFile) {
    _line("Pack: %s", packFile);
  }

  protected final void printPackSize(long packSize) {
    _line("Pack size: %s", FilesizeFormatter.format(packSize));
  }

  protected final void printPackSize(long packSize, long directorySize) {
    var fromSize = FilesizeFormatter.format(directorySize);
    _line("Pack size: %s from %s directory", FilesizeFormatter.format(packSize), fromSize);
  }

  protected final void printCreated(long createdTime) {
    _line("Created: %s", JPackUtils.SDF.format(createdTime));
  }

  protected final void printPackingTime(long elapsedTime) {
    _line("Packing time: %s", JPackUtils.formatMsTime(elapsedTime));
  }

  protected final void printUsedThreads(Number usedThreads) {
    _line("Used threads: %s", usedThreads);
  }

  protected final void printVersion(short version) {
    _line("Version: %s", JPackUtils.versionToStr(version));
  }

  protected final void printSecretKey(String secretKey) {
    _separator();
    _line("Secret key: %s", secretKey);
    _separator();
    _line("Please keep this secret key. The application uses the strongest security.");
    _line("Without the key, you will never be able to recover your data!");
    _line("I am not responsible for data loss. Thank you for understanding.");
  }

  protected final void printLocked() {
    _separator();
    _line("Attention! This pack file requires a secret key to unlock.");
  }
}
