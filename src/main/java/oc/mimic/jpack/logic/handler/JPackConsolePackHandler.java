package oc.mimic.jpack.logic.handler;

import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.logic.JPackService;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.logic.listener.PackListener;
import oc.mimic.jpack.struct.*;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Handler událostí vytvoření packu pro konzoli.
 *
 * @author mimic
 */
public final class JPackConsolePackHandler extends AbstractJPackConsoleHandler implements PackListener {

  private final Map<ProcessState, String> names = new HashMap<>();
  private String directory;
  private int maxLengthBarName;
  private int maxFileSizeDigits;
  private ProcessState currentState;

  public JPackConsolePackHandler() {
    // překlady pro stavy
    names.put(ProcessState.SCANNING, "Scanning...");
    names.put(ProcessState.SCANNING_COMPLETED, "Scanning successed");
    names.put(ProcessState.BUILDING_PACK, "Packing...");
    names.put(ProcessState.READY, "Packing successed");
  }

  public Pack pack(PackConfig config, String inputDirectory, String outputPackFile) {
    directory = inputDirectory;
    try {
      var service = JPackService.createInstance(this, null, null);
      var pack = service.pack(config, inputDirectory, outputPackFile);
      return pack;

    } catch (Exception e) {
      _separator();
      if (e.getMessage() != null) {
        _line("Error: " + e.getMessage());
      } else {
        System.out.println(JPackUtils.stackTraceToString(e));
      }
      _separator();
      return null;
    }
  }

  @Override
  public void stateChanged(ProcessState state) {
    currentState = state;

    if (state == ProcessState.STARTED) {
      var lines = new ArrayList<String>();
      lines.add("Create a pack file from the directory");
      lines.add("");
      lines.add("Directory: " + directory);

      printTitle(lines);
    }
  }

  @Override
  public void directoryScan(File rootDirectory, FileInfo fileInfo, long totalFiles) {
    bar.update(fileInfo.getIndex(), totalFiles, names.get(currentState));
  }

  @Override
  public void directoryScanCompleted(DirectoryInfo dirInfo) {
    bar.done(dirInfo.getFiles().size(), 0, names.get(currentState));

    _plus();
    printTotalFiles(dirInfo.getFiles().size());
    printTotalDirectories(dirInfo.getTotalDirectories());
    printDirectorySize(dirInfo.getTotalFilesize());

    // nutné vypočítat počet znaků pro největší soubor - slouží pro lepší (čistší) výpis vytváření pack souboru
    var max = Long.MIN_VALUE;
    for (var file : dirInfo.getFiles()) {
      if (file.getFilesize() > max) {
        max = file.getFilesize();
      }
    }
    maxFileSizeDigits = (int) (Math.log10(max) + 1);
  }

  @Override
  public void headerPrepared(PackConfig config, PackHeader header) {
    _line();
    printVersion(JPackService.VERSION);
    printCompressMode(config.getCompressMode(), config.getCompressLevel());
    printEncryptMode(config.getEncryptMode());
    printBufferSize(config.getBufferSize());
    printRot128(config.isRot128());
    printShuffle(config.isShuffle());
    printPasswordless(config.isPasswordless());
    printMultithreaded(config.isMultithreaded());
    _plus();
  }

  @Override
  public void writingFile(PackHeaderFile fileHeader, FileProgressInfo progress) {
    var fileProgress = names.get(currentState);

    if (!fileHeader.isDirectory()) {
      var filesize = fileHeader.getFilesize().longValue();
      var fileSizeStr = FilesizeFormatter.format(filesize);
      var read = progress.getPointerPos();
      fileProgress = String
              .format("%s [%" + maxFileSizeDigits + "s/%-" + maxFileSizeDigits + "s] %s ", names.get(currentState),
                      read, filesize, fileSizeStr);
    }
    if (fileProgress.length() > maxLengthBarName) {
      maxLengthBarName = fileProgress.length();
    } else {
      // je tu kvůli promazání konzole, kam se vypisuje průběh, aby tam nezůstali původní znaky
      var diff = maxLengthBarName - fileProgress.length();
      fileProgress += " ".repeat(diff);
    }
    bar.update(progress.getFileNumber() - 1, progress.getTotalFiles(), fileProgress);
  }

  @Override
  public void headerCreated(PackHeader header) {
  }

  @Override
  public void packCreated(Pack pack) {
    var header = pack.getHeaderFull().getHeader();
    var packFile = pack.getPackFile();
    bar.done(header.getTotalFiles().intValue(), maxLengthBarName, names.get(currentState));

    _plus();
    printPack(packFile.getPath());
    printPackSize(packFile.length(), pack.getDirInfo().getTotalFilesize());
    printCreated(header.getCreatedDateTime());
    printPackingTime(header.getPackingTime().longValue());

    if (!pack.getConfig().isPasswordless()) {
      printSecretKey(pack.getSecretKey());
    }
    _plus();
  }
}
