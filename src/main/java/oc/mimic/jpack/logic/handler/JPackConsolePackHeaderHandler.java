package oc.mimic.jpack.logic.handler;

import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.logic.JPackPrinter;
import oc.mimic.jpack.logic.JPackService;
import oc.mimic.jpack.logic.listener.InfoListener;
import oc.mimic.jpack.struct.PackHeaderFull;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;
import java.util.ArrayList;

/**
 * Handler událostí přečtení hlavičky pack souboru pro konzoli.
 *
 * @author mimic
 */
public final class JPackConsolePackHeaderHandler extends AbstractJPackConsoleHandler implements InfoListener {

  private File packFile;
  private boolean printBody;
  private int barMessageLength;

  public void print(String packFile, boolean printBody, String secretKey) {
    this.printBody = printBody;
    this.packFile = new File(packFile);
    try {
      var service = JPackService.createInstance(null, null, this);
      service.readHeader(packFile, secretKey);

    } catch (Exception e) {
      bar.fail("Opening failed");
      bar.done(1, barMessageLength);

      _plus();
      _line("Error: %s", e.getMessage());
      _plus();
    }
  }

  @Override
  public void stateChanged(ProcessState state) {
    if (state == ProcessState.STARTED) {
      var lines = new ArrayList<String>();
      lines.add("Get pack file information");
      lines.add("");
      lines.add("Pack: " + packFile);
      lines.add("Pack size: " + FilesizeFormatter.format(packFile.length()));
      printTitle(lines);

    } else if (state == ProcessState.OPENING_HEADER) {
      var barMessage = "Opening...";
      barMessageLength = barMessage.length();
      bar.update(0, 1, barMessage);
    }
  }

  @Override
  public void headerOpened(File packFile, PackHeaderFull headerFull) {
    var header = headerFull.getHeader();

    bar.done(1, barMessageLength, "Opening successed");

    _plus();
    printTotalFiles(header.getTotalFiles().longValue());
    printTotalDirectories(header.getTotalDirectories());
    printDirectorySize(header.getTotalFilesize().longValue());
    _line();
    printVersion(headerFull.getVersion());
    printCompressMode(header.getCompressModeType(), null);
    printEncryptMode(header.getEncryptModeType());
    printBufferSize(header.getBufferSize().intValue());
    printRot128(header.isRot128());
    printShuffle(header.isShuffle());
    printPasswordless(headerFull.isPasswordless());
    printCreated(header.getCreatedDateTime());
    printPackingTime(header.getPackingTime().longValue());
    printUsedThreads(header.getUsedThreads());

    if (!headerFull.isPasswordless()) {
      printLocked();
    }
    if (printBody) {
      _separator();
      _line("Directory in pack file:");
      _line("");
      JPackPrinter.printPackHeaderFiles(headerFull);
      _separator();
    } else {
      _plus();
    }
  }

  @Override
  public String secretKeyRequired() {
    var key = waitForSecretKey();
    _plus();
    return key;
  }
}
