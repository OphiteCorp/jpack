package oc.mimic.jpack.logic.handler;

import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.logic.JPackService;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.logic.listener.UnpackListener;
import oc.mimic.jpack.struct.*;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Handler událostí rozbalení pack souboru pro konzoli.
 *
 * @author mimic
 */
public final class JPackConsoleUnpackHandler extends AbstractJPackConsoleHandler implements UnpackListener {

  private final Map<ProcessState, String> names = new HashMap<>();
  private File packFile;
  private File outputDirectory;
  private PackHeader header;
  private int maxLengthBarName;
  private ProcessState currentState;
  private String totalSizeStr;

  /**
   * Vytvoření instance.
   */
  public JPackConsoleUnpackHandler() {
    // překlady pro stavy
    names.put(ProcessState.OPENING_HEADER, "Opening header...");
    names.put(ProcessState.OPENING_HEADER_COMPLETED, "Opening header successed");
    names.put(ProcessState.UNPACKING, "Unpacking...");
    names.put(ProcessState.READY, "Unpacking successed");
  }

  public Unpack unpack(UnpackConfig config, String packFile, String outputDirectory) {
    this.packFile = new File(packFile);
    this.outputDirectory = new File(outputDirectory);
    try {
      var service = JPackService.createInstance(null, this, null);
      return service.unpack(config, packFile, outputDirectory);

    } catch (Exception e) {
      _separator();
      if (e.getMessage() != null) {
        _line("Error: " + e.getMessage());
      } else {
        System.out.println(JPackUtils.stackTraceToString(e));
      }
      _separator();
      return null;
    }
  }

  @Override
  public void stateChanged(ProcessState state) {
    currentState = state;

    if (state == ProcessState.STARTED) {
      var lines = new ArrayList<String>();
      lines.add("Unpack the pack file to a directory");
      lines.add("");
      lines.add("Pack: " + packFile);
      lines.add("Pack size: " + FilesizeFormatter.format(packFile.length()));
      lines.add("Output directory: " + outputDirectory);
      printTitle(lines);
    }
  }

  @Override
  public String secretKeyRequired() {
    return waitForSecretKey();
  }

  @Override
  public void headerOpened(File packFile, PackHeaderFull headerFull) {
    var header = headerFull.getHeader();
    this.header = header;
    totalSizeStr = FilesizeFormatter.format(header.getTotalFilesize().longValue());

    printTotalFiles(header.getTotalFiles().longValue());
    printTotalDirectories(header.getTotalDirectories());
    printDirectorySize(header.getTotalFilesize().longValue());
    _line();
    printVersion(headerFull.getVersion());
    printCompressMode(header.getCompressModeType(), null);
    printEncryptMode(header.getEncryptModeType());
    printBufferSize(header.getBufferSize().intValue());
    printRot128(header.isRot128());
    printShuffle(header.isShuffle());
    printPasswordless(headerFull.isPasswordless());
    printCreated(header.getCreatedDateTime());
    printPackingTime(header.getPackingTime().longValue());
    printUsedThreads(header.getUsedThreads());
    _plus();
  }

  @Override
  public void readingPackFile(ReadingPackFileData data, Collection<FileProgressReadInfo> progress) {
    var headerFull = data.getHeaderFull();
    var header = headerFull.getHeader();
    var dt = (int) (Math.log10(JPackService.MAXIMUM_THREADS) + 1);
    var totalReadStr = FilesizeFormatter.format(data.getTotalRead());
    var fileProgress = String
            .format("%s [%10s / %-10s][%" + dt + "s] ", names.get(currentState), totalReadStr, totalSizeStr,
                    data.getActiveThreads());

    if (fileProgress.length() > maxLengthBarName) {
      maxLengthBarName = fileProgress.length();
    }
    bar.update(data.getFilesDone(), header.getTotalFiles().longValue(), fileProgress);
  }

  @Override
  public void unpackCompleted(Unpack unpack) {
    bar.done(header.getTotalFiles().intValue(), maxLengthBarName, names.get(currentState));

    _plus();
    _line("Unpack time: %s", JPackUtils.formatMsTime(unpack.getElapsedTime()));
    _plus();
  }
}
