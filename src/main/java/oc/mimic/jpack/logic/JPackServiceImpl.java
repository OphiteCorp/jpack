package oc.mimic.jpack.logic;

import oc.mimic.jpack.component.AesGcmCipher;
import oc.mimic.jpack.helper.IOBytesHelper;
import oc.mimic.jpack.helper.IOBytesStream;
import oc.mimic.jpack.logic.exception.InvalidOpenPackHeaderException;
import oc.mimic.jpack.logic.exception.InvalidSecretKeyException;
import oc.mimic.jpack.logic.exception.JPackException;
import oc.mimic.jpack.logic.listener.InfoListener;
import oc.mimic.jpack.logic.listener.PackListener;
import oc.mimic.jpack.logic.listener.UnpackListener;
import oc.mimic.jpack.logic.task.ProcessFilePartResult;
import oc.mimic.jpack.logic.task.ProcessFilePartTask;
import oc.mimic.jpack.logic.task.ReadSingleFileListener;
import oc.mimic.jpack.logic.task.ReadSingleFileTask;
import oc.mimic.jpack.struct.*;
import oc.mimic.jpack.struct.enums.ProcessState;
import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.XZInputStream;
import org.tukaani.xz.XZOutputStream;

import javax.crypto.AEADBadTagException;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;

/**
 * Implementace pro pack/unpack adresáře.
 *
 * @author mimic
 */
final class JPackServiceImpl implements JPackService {

  private static final int SHUFFLE_SECRET_LENGTH = Long.BYTES;

  private final PackListener packListener;
  private final UnpackListener unpackListener;
  private final InfoListener infoListener;

  /**
   * Vytvoří novou instanci služby.
   */
  public JPackServiceImpl() {
    packListener = null;
    unpackListener = null;
    infoListener = null;
  }

  /**
   * Vytvoří novou instanci služby.
   *
   * @param packListener   Listener pro odchycení událostí pro pack souboru.
   * @param unpackListener Listener pro odchycení událostí pro unpack pack souboru.
   * @param infoListener   Listener pro odchycení událostí pro info pack souboru.
   */
  public JPackServiceImpl(PackListener packListener, UnpackListener unpackListener, InfoListener infoListener) {
    this.packListener = packListener;
    this.unpackListener = unpackListener;
    this.infoListener = infoListener;
  }

  @Override
  public synchronized Pack pack(PackConfig config, String inputDirectory, String outputPackFile) {
    var startTime = System.currentTimeMillis();
    onStateChanged(ProcessState.STARTED);
    // pokud vstupní adresář neexistuje nebo se nejedná o adresář, tak chyba
    var inputDir = new File(inputDirectory);
    if (!inputDir.exists() || !inputDir.isDirectory()) {
      throw new JPackException("Input directory does not exist or is not a directory: " + inputDirectory);
    }
    // pokud výstupní pack soubor již existuje, tak se smaže, případně vytvoří adresáře k novému pack souboru
    var packFile = new File(outputPackFile);
    if (packFile.exists()) {
      if (packFile.isFile()) {
        packFile.delete();
      } else {
        throw new JPackException(
                "Wrong output pack file. There must be a file and no directory. The output pack file is: " +
                outputPackFile);
      }
    } else {
      packFile.getParentFile().mkdirs();
    }
    // projde cílový adresář a vytvoří informace o adresáři a hlavičku pro pack soubor
    var dirInfo = getDirectoryInfo(inputDir);
    var header = createPackHeader(config, dirInfo);
    var pack = preparePack(packFile, config);
    pack.setDirInfo(dirInfo);

    // detailní informace o hlavičce
    var headerFull = new PackHeaderFull();
    headerFull.setHeader(header);
    headerFull.setPasswordless(config.isPasswordless());
    headerFull.setSecretKey(AesGcmCipher.toHexBytes(pack.getSecretKey()));

    // vytvoření pack souboru
    try (var raf = new RandomAccessFile(outputPackFile, "rw")) {
      writePackFileBody(config, raf, dirInfo, header, pack.getSecretKey());
      header.setPackingTime(System.currentTimeMillis() - startTime);
      writePackFileFooter(config, raf, header, pack);

    } catch (JPackException e) {
      throw e;
    } catch (Exception e) {
      throw new JPackException("An unexpected error occurred while creating the pack file: " + outputPackFile, e);
    } finally {
      onStateChanged(ProcessState.READY);
    }
    if (config.isDeleteDirectoryAfterFinish()) {
      deleteDirectory(dirInfo.getDirectory());
    }
    pack.setHeaderFull(headerFull);
    pack.setElapsedTime(System.currentTimeMillis() - startTime);
    onPackCreated(pack);
    return pack;
  }

  @Override
  public synchronized Unpack unpack(UnpackConfig config, String inputPackFile, String outputDirectory) {
    var startTime = System.currentTimeMillis();
    onStateChanged(ProcessState.STARTED);
    var packFile = getValidPackFile(inputPackFile);
    // výstupní adresář musí existovat
    var outputDir = new File(outputDirectory);
    if (outputDir.exists()) {
      if (outputDir.isFile()) {
        throw new JPackException("The output directory cannot be a file. Output directory is: " + outputDirectory);
      }
    } else {
      outputDir.mkdirs();
    }
    var unpack = new Unpack();
    unpack.setConfig(config);
    unpack.setPackFile(packFile);
    unpack.setOutputDirectory(outputDir);
    // otevře pack soubor a začne číst
    try (var raf = new RandomAccessFile(packFile, "r")) {
      onStateChanged(ProcessState.OPENING_HEADER);
      // načte magický byte
      checkMagicBytes(raf);

      var endOfHeaderLength = JPackService.MAGIC_BYTES.length; // bude určovat konec těla pack souboru
      // vytáhne délku hlavičky z konce pack souboru
      raf.seek(raf.length() - endOfHeaderLength - 1);
      var headerLengthData = JPackUtils.readNumber(raf, true);
      var headerLength = headerLengthData.getNumber().intValue();
      endOfHeaderLength += headerLengthData.getNumberLength() + headerLength;
      raf.seek(raf.length() - endOfHeaderLength);

      // načte hlavičku
      var headerBytes = new byte[headerLength];
      raf.read(headerBytes);
      var headerFull = createPackHeader(headerBytes, config.getSecretKey());
      var header = headerFull.getHeader();
      unpack.setHeaderFull(headerFull);
      onStateChanged(ProcessState.OPENING_HEADER_COMPLETED);
      onHeaderOpened(packFile, headerFull);
      raf.seek(0L);

      var executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(JPackService.MAXIMUM_THREADS);
      onStateChanged(ProcessState.UNPACKING);

      var readingData = new ReadingPackFileData();
      readingData.setHeaderFull(headerFull);

      // implementace listeneru, který zachytí všechny události při čtení všech souborů
      // je voláno z více vláken, takže je potřeba všechno dělat thread-safe
      var readSingleFileHandle = new ReadSingleFileListener() {

        private volatile Map<Long, FileProgressReadInfo> readingFiles = new HashMap<>(JPackService.MAXIMUM_THREADS);
        private volatile Set<Long> finishedCounter = new HashSet<>();
        private volatile long totalRead;

        {
          finishedCounter.add(Long.MIN_VALUE); // +1 je kvůli absenci posledního volání události
          // je tu z důvodu, že prázdné adresáře se vytvoří automaticky jen pomocí hlavičky, takže se na ně tato
          // událost nevztahuje - proto je "přidáme" ručně
          for (var headerFile : header.getFiles()) {
            if (headerFile.isDirectory()) {
              finishedCounter.add(headerFile.getIndex());
            }
          }
        }

        @Override
        public void updateProgress(ReadSingleFileTask task, FileProgressReadInfo progress) {
          synchronized (this) {
            totalRead += progress.getCurrentRead();
            readingFiles.put(progress.getHeaderFile().getIndex(), progress);

            readingData.setFilesDone(finishedCounter.size());
            readingData.setTotalRead(totalRead);
            readingData.setActiveThreads(config.isMultithreaded() ? executorService.getActiveCount() : 1);

            onReadingPackFile(readingData, readingFiles.values());

            readingFiles.values().removeIf(FileProgressReadInfo::isFinished);
            if (progress.isFinished()) {
              finishedCounter.add(progress.getHeaderFile().getIndex());
            }
          }
        }
      };
      // projde soubory z hlavičky a rozbalí je
      for (var i = 0; i < header.getFiles().size(); i++) {
        var headerFile = header.getFiles().get(i);
        var outputFile = Paths.get(outputDir.getPath(), headerFile.getRelativePath()).toFile();

        if (headerFile.isDirectory()) {
          if (!outputFile.exists()) {
            outputFile.mkdirs();
          }
          // pokud se jedná pouze o adresář, tak ho vytvoříme a přejdeme na další soubor
          continue;
        } else {
          if (!outputFile.getParentFile().exists()) {
            outputFile.getParentFile().mkdirs();
          }
        }
        // zjistí délku souboru v pack souboru podle entryPointu
        long entryPointLength;
        var nextEntryPoint = nextFileEntryPoint(headerFull, i);
        if (nextEntryPoint != -1) {
          entryPointLength = nextEntryPoint - headerFile.getEntryPoint();
        } else {
          entryPointLength = raf.length() - headerFile.getEntryPoint() - endOfHeaderLength;
        }
        var task = new ReadSingleFileTask(packFile, outputFile, entryPointLength, headerFull, headerFile);
        if (unpackListener != null) {
          task.addListener(readSingleFileHandle);
        }
        if (config.isMultithreaded()) {
          executorService.execute(task);
        } else {
          task.run();
        }
      }
      executorService.shutdown();
      executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

    } catch (JPackException e) {
      throw e;
    } catch (Exception e) {
      throw new JPackException("An unexpected error occurred while processing the pack file: " + inputPackFile, e);
    } finally {
      onStateChanged(ProcessState.READY);
    }
    if (config.isDeletePackAfterFinish()) {
      deletePackFile(packFile);
    }
    unpack.setElapsedTime(System.currentTimeMillis() - startTime);
    onUnpackCompleted(unpack);
    return unpack;
  }

  @Override
  public PackHeaderFull readHeader(String inputPackFile, String secretKey) {
    onStateChanged(ProcessState.STARTED);
    // vstupní pack soubor musí existovat a musí být možno z něho číst
    var packFile = getValidPackFile(inputPackFile);
    // otevře pack soubor a začne číst
    try (var raf = new RandomAccessFile(packFile, "r")) {
      // načte magický byte
      checkMagicBytes(raf);
      // vytáhne délku hlavičky z konce pack souboru
      raf.seek(raf.length() - JPackService.MAGIC_BYTES.length - 1);
      var headerLengthData = JPackUtils.readNumber(raf, true);
      var headerLength = headerLengthData.getNumber().intValue();
      raf.seek(raf.length() - JPackService.MAGIC_BYTES.length - headerLengthData.getNumberLength() - headerLength);

      // načte hlavičku
      var headerBytes = new byte[headerLength];
      raf.read(headerBytes);
      var headerFull = createPackHeader(headerBytes, secretKey);
      onStateChanged(ProcessState.OPENING_HEADER_COMPLETED);
      onHeaderOpened(packFile, headerFull);
      return headerFull;

    } catch (JPackException e) {
      throw e;
    } catch (Exception e) {
      throw new InvalidOpenPackHeaderException("Unable to load pack header from: " + inputPackFile, e);
    } finally {
      onStateChanged(ProcessState.READY);
    }
  }

  /**
   * Nastala změna stavu.
   *
   * @param state Aktuální stav.
   */
  private void onStateChanged(ProcessState state) {
    if (packListener != null) {
      packListener.stateChanged(state);
    }
    if (unpackListener != null) {
      unpackListener.stateChanged(state);
    }
    if (infoListener != null) {
      infoListener.stateChanged(state);
    }
  }

  /**
   * Skenování adresáře pro budoucí pack soubor.
   *
   * @param rootDirectory Hlavní adresář.
   * @param fileInfo      Informace o nalazeném souboru nebo adresáři.
   * @param totalFiles    Celkový počet souborů a adresářů.
   */
  private void onDirectoryScan(File rootDirectory, FileInfo fileInfo, long totalFiles) {
    if (packListener != null) {
      packListener.directoryScan(rootDirectory, fileInfo, totalFiles);
    }
  }

  /**
   * Skenování adresáře bylo dokončeno.
   *
   * @param dirInfo Informace o adresáři.
   */
  private void onDirectoryScanCompleted(DirectoryInfo dirInfo) {
    if (packListener != null) {
      packListener.directoryScanCompleted(dirInfo);
    }
  }

  /**
   * Hlavička pack souboru byla připravena. V tuto chvíli stále nemusí být kompletní.
   *
   * @param config Konfigurace pack souboru.
   * @param header Hlavička pack souboru.
   */
  private void onHeaderPrepared(PackConfig config, PackHeader header) {
    if (packListener != null) {
      packListener.headerPrepared(config, header);
    }
  }

  /**
   * Zápis souboru do pack souboru.
   *
   * @param fileHeader Informace o souboru, který se zapisuje do pack.
   * @param progress   Aktuální průběh.
   */
  private void onWritingFile(PackHeaderFile fileHeader, FileProgressInfo progress) {
    if (packListener != null) {
      packListener.writingFile(fileHeader, progress);
    }
  }

  /**
   * Hlavička pack souboru byla kompletně vytvořena.
   *
   * @param header Hlavička pack souboru.
   */
  private void onHeaderCreated(PackHeader header) {
    if (packListener != null) {
      packListener.headerCreated(header);
    }
  }

  /**
   * Pack soubor byl vytvořen.
   *
   * @param pack Informace o pack souboru.
   */
  private void onPackCreated(Pack pack) {
    if (packListener != null) {
      packListener.packCreated(pack);
    }
  }

  /**
   * Hlavička pack souboru byla otevřena.
   *
   * @param packFile   Pack soubor.
   * @param headerFull Hlavička pack souboru.
   */
  private void onHeaderOpened(File packFile, PackHeaderFull headerFull) {
    if (unpackListener != null) {
      unpackListener.headerOpened(packFile, headerFull);
    }
    if (infoListener != null) {
      infoListener.headerOpened(packFile, headerFull);
    }
  }

  /**
   * Pack vyžaduje tajný klíč, který nebyl nalazen mezi parametry aplikace.
   *
   * @return Tajný klíč pro odemčení packu.
   */
  private String onSecretKeyRequired() {
    if (unpackListener != null) {
      return unpackListener.secretKeyRequired();
    }
    if (infoListener != null) {
      return infoListener.secretKeyRequired();
    }
    return null;
  }

  /**
   * Probíhá čtení pack souboru.
   *
   * @param data     Informace o průběhu čtení.
   * @param progress Aktuální průběh. Obsahuje všechny běžící procesy pro více vláken a obsahuje pouze nedokončené.
   *                 Soubor, který byl již kompletně načten z kolekce zmizí. Všechna logika musí být thread-safe.
   */
  private void onReadingPackFile(ReadingPackFileData data, Collection<FileProgressReadInfo> progress) {
    if (unpackListener != null) {
      unpackListener.readingPackFile(data, progress);
    }
  }

  /**
   * Rozbalení pack souboru bylo dokončeno.
   *
   * @param unpack Informace o rozbaleném pack souboru.
   */
  private void onUnpackCompleted(Unpack unpack) {
    if (unpackListener != null) {
      unpackListener.unpackCompleted(unpack);
    }
  }

  /**
   * Připraví výstupní pack objekt.
   *
   * @param packFile Výstupní pack soubor.
   * @param config   Konfigurace pack souboru.
   *
   * @return Nová instance packu.
   */
  private Pack preparePack(File packFile, PackConfig config) {
    onStateChanged(ProcessState.BUILDING_PACK);
    byte[] secretBytes;

    if (config.getSecretKey() == null) {
      secretBytes = AesGcmCipher.generateSecretKey();
    } else {
      JPackUtils.checkSecretKey(config.getSecretKey());
      secretBytes = AesGcmCipher.toHexBytes(config.getSecretKey());
    }
    var pack = new Pack();
    pack.setConfig(config);
    pack.setPackFile(packFile);
    pack.setSecretKey(AesGcmCipher.toHex(secretBytes));
    return pack;
  }

  /**
   * Získá informace o adresáři.
   *
   * @param inputDirectory Cesta k adresáři.
   *
   * @return Informace o adresáři.
   */
  private DirectoryInfo getDirectoryInfo(File inputDirectory) {
    onStateChanged(ProcessState.SCANNING);

    var absolutePath = inputDirectory.getAbsolutePath();
    var dirInfo = new DirectoryInfo(inputDirectory);
    var totalFilesize = 0L;
    var totalDirectories = 0L;
    var fileIndex = 0L;
    var totalFiles = getTotalFiles(inputDirectory);

    var stack = new Stack<File>();
    stack.push(inputDirectory);

    while (!stack.isEmpty()) {
      var dir = stack.pop();
      var files = dir.listFiles();

      if (files != null) {
        for (var file : files) {
          var relativePath = file.getAbsolutePath().substring(absolutePath.length() + 1);
          var fileInfo = new FileInfo(file.toPath(), fileIndex++);
          fileInfo.setRelativePath(relativePath);

          if (file.isDirectory()) {
            stack.push(file);
            totalDirectories++;
            fileInfo.setDirectory(true);
            fileInfo.setFilesize(0L);
          } else {
            fileInfo.setFilesize(file.length());
            totalFilesize += file.length();
          }
          dirInfo.addFile(fileInfo);
          onDirectoryScan(inputDirectory, fileInfo, totalFiles);
        }
      }
    }
    dirInfo.setTotalFilesize(totalFilesize);
    dirInfo.setTotalDirectories(totalDirectories);

    onStateChanged(ProcessState.SCANNING_COMPLETED);
    onDirectoryScanCompleted(dirInfo);
    return dirInfo;
  }

  /**
   * Získá celkový počet všech souborů a adresářů.
   *
   * @param inputDirectory Vstupní adresář.
   *
   * @return Počet souborů včetně adresářů.
   */
  private long getTotalFiles(File inputDirectory) {
    try (var walk = Files.walk(inputDirectory.toPath()).parallel()) {
      return Math.max(walk.count() - 1, 0);
    } catch (Exception e) {
      throw new JPackException(
              "There was an error getting the number of files and directories from: " + inputDirectory);
    }
  }

  /**
   * Převede pole bytů reprezentující hlavičku pack souboru na objekt.
   *
   * @param bytes     Hlavička pack souboru jako pole bytů.
   * @param secretKey Tajný klíč pro decrypt hlavičky.
   *
   * @return Nová instance hlavičky pack souboru.
   */
  private PackHeaderFull createPackHeader(byte[] bytes, String secretKey) {
    var headerFull = new PackHeaderFull();

    try (var bais = new ByteArrayInputStream(IOBytesHelper.rot128(bytes))) {
      // na začátku je IV, které je nutné pro decrypt
      var iv = new byte[AesGcmCipher.GCM_IV_LENGTH];
      bais.read(iv);

      byte[] secretKeyBytes;
      var passwordless = IOBytesHelper.fromRawBoolean((byte) bais.read());
      var version = IOBytesHelper.fromRawShort(bais.readNBytes(Short.BYTES));

      // pokud bude pack bez hesla, tak je heslo (tajný klíč) uložen přímo v packu
      if (passwordless) {
        var secretKeyLength = (byte) bais.read();
        secretKeyBytes = IOBytesHelper.rot128(bais.readNBytes(secretKeyLength));
      } else {
        // pokud tajný klíč na vstupu nebude existovat, tak se vyžádá
        if (secretKey == null) {
          secretKey = onSecretKeyRequired();
        }
        JPackUtils.checkSecretKey(secretKey);
        secretKeyBytes = AesGcmCipher.toHexBytes(secretKey);
      }
      // speciálně pro info, aby byla možnost detekovat správné zobrazení progress baru do konzole
      if (infoListener != null) {
        onStateChanged(ProcessState.OPENING_HEADER);
      }
      var cipher = AesGcmCipher.getDecryptCipher(secretKeyBytes, iv);

      headerFull.setSecretKey(secretKeyBytes);
      headerFull.setPasswordless(passwordless);
      headerFull.setVersion(version);

      var header = new PackHeader();
      headerFull.setHeader(header);

      try (var cis = new CipherInputStream(bais, cipher)) {
        try (var xz = new XZInputStream(cis)) {
          try (var ibs = new IOBytesStream.Input(xz)) {
            var totalFiles = ibs.readNumber();
            var totalFilesize = ibs.readNumber();
            var totalDirectories = ibs.readNumber();
            var bufferSize = ibs.readNumber();
            var compressMode = ibs.readRawByte();
            var encryptMode = ibs.readRawByte();
            var rot128 = ibs.readRawBoolean();
            var shuffle = ibs.readRawBoolean();
            var createdDateTime = ibs.readRawLong();
            var packingTime = ibs.readNumber();
            var usedThreads = ibs.readNumber();
            var fileIndex = 0L;

            header.setTotalFiles(totalFiles);
            header.setTotalFilesize(totalFilesize);
            header.setTotalDirectories(totalDirectories);
            header.setBufferSize(bufferSize);
            header.setCompressMode(compressMode);
            header.setEncryptMode(encryptMode);
            header.setRot128(rot128);
            header.setShuffle(shuffle);
            header.setCreatedDateTime(createdDateTime);
            header.setPackingTime(packingTime);
            header.setUsedThreads(usedThreads);

            // projde jednotlivé soubory a získá o nich informace
            for (var i = 0; i < totalFiles.longValue(); i++) {
              var relativePath = ibs.readString();
              var fileSize = ibs.readNumber();
              var directory = ibs.readRawBoolean();
              var entryPoint = ibs.readRawLong();

              byte[] shuffleSecret = null;
              if (header.isShuffle()) {
                shuffleSecret = ibs.readNBytes(SHUFFLE_SECRET_LENGTH);
              }
              var headerFile = new PackHeaderFile(fileIndex++);
              headerFile.setRelativePath(relativePath);
              headerFile.setFilesize(fileSize);
              headerFile.setEntryPoint(entryPoint);
              headerFile.setDirectory(directory);
              headerFile.setShuffleSecret(shuffleSecret);
              header.getFiles().add(headerFile);
            }
          }
        }
      }
    } catch (JPackException e) {
      throw e;
    } catch (Exception e) {
      if (e.getCause() instanceof AEADBadTagException) {
        throw new InvalidSecretKeyException("The secret key is not valid.", e);
      }
      throw new JPackException("An unexpected error occurred while reading pack pack header.", e);
    }
    return headerFull;
  }

  /**
   * Vytvoří hlavičku pack souboru.
   *
   * @param config  Konfigurace pack souboru.
   * @param dirInfo Informace o adresáři.
   *
   * @return Hlavička pack souboru.
   */
  private PackHeader createPackHeader(PackConfig config, DirectoryInfo dirInfo) {
    onStateChanged(ProcessState.PREPARING_PACK_HEADER);

    var header = new PackHeader();
    header.setTotalFiles(dirInfo.getFiles().size());
    header.setTotalFilesize(dirInfo.getTotalFilesize());
    header.setTotalDirectories(dirInfo.getTotalDirectories());
    header.setBufferSize(config.getBufferSize());
    header.setCompressMode(config.getCompressMode().getCode());
    header.setEncryptMode(config.getEncryptMode().getCode());
    header.setRot128(config.isRot128());
    header.setShuffle(config.isShuffle());
    header.setCreatedDateTime(new Date().getTime());
    header.setPackingTime(null); // bude vyplněn později
    header.setUsedThreads(config.isMultithreaded() ? JPackService.MAXIMUM_THREADS : 1);

    for (var fileInfo : dirInfo.getFiles()) {
      var headerFile = new PackHeaderFile(fileInfo.getIndex());
      headerFile.setRelativePath(fileInfo.getRelativePath());
      headerFile.setFilesize(fileInfo.getFilesize());
      headerFile.setDirectory(fileInfo.isDirectory());

      if (header.isShuffle()) {
        var shuffleSecret = AesGcmCipher.generateSalt(SHUFFLE_SECRET_LENGTH);
        headerFile.setShuffleSecret(shuffleSecret);
      }
      headerFile.setEntryPoint(0L); // zatím bude 0, protože zatím neznáme pozici v pack souboru
      header.getFiles().add(headerFile);
    }
    onStateChanged(ProcessState.PREPARING_PACK_HEADER_COMPLETED);
    onHeaderPrepared(config, header);
    return header;
  }

  /**
   * Převede hlavičku pack souboru do pole bytů.
   *
   * @param header    Hlavička pack souboru.
   * @param secretKey Tajný klíč pro encrypt hlavičky.
   * @param config    Konfigurace pack souboru.
   *
   * @return Hlavička packu jako pole bytů.
   */
  private byte[] toPackHeaderBytes(PackHeader header, String secretKey, PackConfig config) {
    byte[] bytes;

    try (var baos = new ByteArrayOutputStream()) {
      // nebude použit tajný klíč
      var secretBytes = AesGcmCipher.toHexBytes(secretKey);
      var cipher = AesGcmCipher.getEncryptCipher(secretBytes);

      baos.write(cipher.getIV()); // jako první zapíšeme IV, které jsou nutné pro decrypt
      baos.write(IOBytesHelper.toRawByte(config.isPasswordless())); // jako druhý, zda se má vložit i klíč
      baos.write(IOBytesHelper.toRawBytes(JPackService.VERSION)); // jako třetí bude verze aplikace (packu)

      // pokud bude pack bez hesla, tak vloží i tajný klíč
      if (config.isPasswordless()) {
        baos.write(IOBytesHelper.toRawBytes((byte) secretBytes.length));
        baos.write(IOBytesHelper.rot128(secretBytes));
      }
      try (var cos = new CipherOutputStream(baos, cipher)) {
        var options = new LZMA2Options();
        options.setPreset(LZMA2Options.PRESET_MAX);

        try (var xz = new XZOutputStream(cos, options)) {
          try (var obs = new IOBytesStream.Output(xz)) {
            obs.writeNumber(header.getTotalFiles());
            obs.writeNumber(header.getTotalFilesize());
            obs.writeNumber(header.getTotalDirectories());
            obs.writeNumber(header.getBufferSize());
            obs.writeRawByte(header.getCompressMode());
            obs.writeRawByte(header.getEncryptMode());
            obs.writeRawBoolean(header.isRot128());
            obs.writeRawBoolean(header.isShuffle());
            obs.writeRawLong(header.getCreatedDateTime());
            obs.writeNumber(header.getPackingTime());
            obs.writeNumber(header.getUsedThreads());

            for (var file : header.getFiles()) {
              obs.writeString(file.getRelativePath());
              obs.writeNumber(file.getFilesize());
              obs.writeRawBoolean(file.isDirectory());
              obs.writeRawLong(file.getEntryPoint());

              if (header.isShuffle()) {
                obs.write(file.getShuffleSecret());
              }
            }
          }
        }
      }
      bytes = IOBytesHelper.rot128(baos.toByteArray());

    } catch (JPackException e) {
      throw e;
    } catch (Exception e) {
      throw new JPackException("An unexpected error occurred while converting pack file header to bytes.", e);
    }
    return bytes;
  }

  /**
   * Zapíše do pack souboru soubory z adresáře (tělo).
   *
   * @param config    Konfigurace pack souboru.
   * @param raf       Výstupní stream na pack soubor.
   * @param dirInfo   Informace o adresáři včetně souborů.
   * @param header    hlavička pack souboru.
   * @param secretKey Tajný klíč.
   */
  private void writePackFileBody(PackConfig config, RandomAccessFile raf, DirectoryInfo dirInfo, PackHeader header,
          String secretKey) throws Exception {

    var buffer = new byte[config.getBufferSize() * 2]; // musí být rezerva
    var executorService = (ThreadPoolExecutor) Executors.newFixedThreadPool(JPackService.MAXIMUM_THREADS);
    var progressInfo = new FileProgressInfo();
    progressInfo.setTotalFiles(dirInfo.getFiles().size());

    // zapíšeme jednotlivé soubory
    for (var i = 0; i < dirInfo.getFiles().size(); i++) {
      var fileInfo = dirInfo.getFiles().get(i);
      progressInfo.setFileNumber(i + 1);
      progressInfo.setPointerPos(0L);

      // nastavíme entryPoint souboru, na kterém bude v pack souboru začínat
      PackHeaderFile headerFile;
      var headerFileOpt = header.getFiles().stream().filter(p -> p.getIndex() == fileInfo.getIndex()).findAny();
      if (headerFileOpt.isPresent()) {
        headerFile = headerFileOpt.get();
        headerFile.setEntryPoint(Math.max(raf.getFilePointer(), raf.length()));
      } else {
        throw new JPackException("No header file found by index: " + fileInfo.getIndex());
      }
      // adresáře (většinou prázdné) přeskočíme, ty budou uložené pouze v hlavičce
      if (fileInfo.isDirectory()) {
        onWritingFile(headerFile, progressInfo);
        continue;
      }
      var file = fileInfo.getFilePath().toFile();
      var callables = new ArrayList<Callable<ProcessFilePartResult>>(JPackService.MAXIMUM_THREADS);

      // otevře soubor a zapíše ho do pack souboru
      try (var rafFile = new RandomAccessFile(file, "r")) {
        var secretKeyBytes = AesGcmCipher.toHexBytes(secretKey);
        int read;

        while ((read = rafFile.read(buffer, 0, config.getBufferSize())) != -1) {
          var data = Arrays.copyOfRange(buffer, 0, read);
          var task = new ProcessFilePartTask(config, header, headerFile, secretKeyBytes, data);

          progressInfo.setPointerPos(rafFile.getFilePointer());
          onWritingFile(headerFile, progressInfo);

          if (config.isMultithreaded()) {
            // zpracování je více-vláknový
            callables.add(task);

            if (callables.size() == JPackService.MAXIMUM_THREADS) {
              invokeFutures(executorService, raf, callables);
            }
          } else {
            // zpracování je jedno-vláknový
            var result = task.call();
            writeTaskResultToPack(raf, result);
          }
        }
        if (config.isMultithreaded()) {
          // nutné dokončit zbytek, na který nebyl prostor pči čtení souboru, protože se callable vytváří podle
          // velikosti executoru
          invokeFutures(executorService, raf, callables);
        }
      }
    }
    executorService.shutdown();
    executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
  }

  /**
   * Zapíše do pack souboru patičku, kde je umístěna hlavička pack souboru.
   *
   * @param config Konfigurace pack souboru.
   * @param raf    Výstupní stream na pack soubor.
   * @param header hlavička pack souboru.
   * @param pack   Informace o pack souboru.
   */
  private void writePackFileFooter(PackConfig config, RandomAccessFile raf, PackHeader header, Pack pack)
          throws IOException {

    // po zapsání všech souborů převedeme hlavičku to pole bytů a zapíšeme na konec souboru
    var headerBytes = toPackHeaderBytes(header, pack.getSecretKey(), config);
    raf.write(headerBytes);

    // zapíše velikost hlavičky
    var headerLengthBytes = IOBytesHelper.toNumberBytes(headerBytes.length);
    IOBytesHelper.reverse(headerLengthBytes); // flag typu musí být na konci
    raf.write(headerLengthBytes); // zapíše délku hlavičky (může být od byte do long)
    raf.write(JPackService.MAGIC_BYTES); // zapíše identifikaci packu

    onStateChanged(ProcessState.CREATED_PACK_HEADER);
    onHeaderCreated(header);
  }

  /**
   * Provolá všechny callable tásky a počká, než se všechny zpracují.
   *
   * @param executorService Služba pro zpracování tásků.
   * @param raf             Otevřený pack soubor pro zápis.
   * @param callables       Callable tásky.
   */
  private void invokeFutures(ExecutorService executorService, RandomAccessFile raf,
          List<Callable<ProcessFilePartResult>> callables)
          throws ExecutionException, InterruptedException, IOException {

    if (!callables.isEmpty()) {
      var futureList = executorService.invokeAll(callables);

      for (var future : futureList) {
        var result = future.get();
        writeTaskResultToPack(raf, result);
      }
      callables.clear();
    }
  }

  /**
   * Zapíše do pack souboru výsledek po zpracování.
   *
   * @param raf    Otevřený pack soubor určený pro zápis.
   * @param result Výsledek po zpracování.
   */
  private void writeTaskResultToPack(RandomAccessFile raf, ProcessFilePartResult result) throws IOException {
    // pokud se velikost dat zmenila
    if (result.isDataLengthSet()) {
      raf.write(IOBytesHelper.toNumberBytes(result.getDataLength()));

      // zapíše původní velikost dat před kompresí - urychlí pozdější dekompresi
      if (result.isDecompressDataLengthSet()) {
        raf.write(IOBytesHelper.toNumberBytes(result.getDecompressDataLength()));
      }
    }
    raf.write(result.getData());
  }

  /**
   * Získá následující entryPoint souboru.
   *
   * @param headerFull Hlavička pack souboru.
   * @param index      Aktuální index v cyklu.
   *
   * @return EntryPoint následujícího souboru. Pokud už žádný neexistuje, tak vrací -1.
   */
  private static long nextFileEntryPoint(PackHeaderFull headerFull, int index) {
    var header = headerFull.getHeader();

    if (index < header.getFiles().size() - 1) {
      for (var i = index + 1; i < header.getFiles().size(); i++) {
        var headerFile = header.getFiles().get(i);

        if (!headerFile.isDirectory()) {
          return headerFile.getEntryPoint();
        }
      }
    }
    return -1;
  }

  /**
   * Načte a zkontroluje magický byte, který definuje tento typ pack souboru.
   *
   * @param raf Otevřený pack soubor.
   */
  private static void checkMagicBytes(RandomAccessFile raf) throws IOException {
    // načte magický byte
    var magicBytes = new byte[JPackService.MAGIC_BYTES.length];
    raf.seek(raf.length() - JPackService.MAGIC_BYTES.length);
    raf.read(magicBytes);

    if (Arrays.compare(magicBytes, JPackService.MAGIC_BYTES) != 0) {
      throw new InvalidOpenPackHeaderException("Invalid pack file type.");
    }
  }

  /**
   * Získá platný pack soubor.
   *
   * @param packFile Cesta k pack souboru.
   *
   * @return Instance souboru.
   */
  private static File getValidPackFile(String packFile) {
    // vstupní pack soubor musí existovat a musí být možno z něho číst
    var file = new File(packFile);
    if (!file.exists() || !file.isFile() || !file.canRead()) {
      throw new JPackException(
              "Input pack file not found or not readable. Check that the file path and permissions are correct. Pack file: " +
              packFile);
    }
    return file;
  }

  /**
   * Pokusí se smazat celý adresář včetně všech souborů a adresářů.
   *
   * @param directory Vstupní adresář.
   */
  private static void deleteDirectory(File directory) {
    try {
      var path = directory.toPath();
      Files.walk(path).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);

      if (Files.exists(path)) {
        throw new JPackException("The directory '" + directory +
                                 "' was not completely deleted, there was still something left that could not be deleted.");
      }
    } catch (IOException e) {
      throw new JPackException("There was an error deleting the directory: " + directory);
    }
  }

  /**
   * Pokusí se smazat pack soubor.
   *
   * @param packFile Pack soubor.
   */
  private static void deletePackFile(File packFile) {
    try {
      packFile.delete();
    } catch (Exception e) {
      throw new JPackException("There was an error deleting the pack file: " + packFile);
    }
  }
}
