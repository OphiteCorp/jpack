package oc.mimic.jpack.logic.listener;

/**
 * Listener pro odchycení událostí při práci s tajným klíčem.
 *
 * @author mimic
 */
public interface SecretKeyListener {

  /**
   * Pack vyžaduje tajný klíč, který nebyl nalazen mezi parametry aplikace.
   *
   * @return Tajný klíč pro odemčení packu.
   */
  String secretKeyRequired();
}
