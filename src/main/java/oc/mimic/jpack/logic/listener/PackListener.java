package oc.mimic.jpack.logic.listener;

import oc.mimic.jpack.struct.*;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;

/**
 * Listener pro odchycení událostí při pack souboru.
 *
 * @author mimic
 */
public interface PackListener {

  /**
   * Nastala změna stavu.
   *
   * @param state Aktuální stav.
   */
  void stateChanged(ProcessState state);

  /**
   * Skenování adresáře pro budoucí pack soubor.
   *
   * @param rootDirectory Hlavní adresář.
   * @param fileInfo      Informace o nalazeném souboru nebo adresáři.
   * @param totalFiles    Celkový počet souborů a adresářů.
   */
  void directoryScan(File rootDirectory, FileInfo fileInfo, long totalFiles);

  /**
   * Skenování adresáře bylo dokončeno.
   *
   * @param dirInfo Informace o adresáři.
   */
  void directoryScanCompleted(DirectoryInfo dirInfo);

  /**
   * Hlavička pack souboru byla připravena. V tuto chvíli stále nemusí být kompletní.
   *
   * @param config Konfigurace pack souboru.
   * @param header Hlavička pack souboru.
   */
  void headerPrepared(PackConfig config, PackHeader header);

  /**
   * Zápis souboru do pack souboru.
   *
   * @param fileHeader Informace o souboru, který se zapisuje do pack.
   * @param progress   Aktuální průběh.
   */
  void writingFile(PackHeaderFile fileHeader, FileProgressInfo progress);

  /**
   * Hlavička pack souboru byla kompletně vytvořena.
   *
   * @param header Hlavička pack souboru.
   */
  void headerCreated(PackHeader header);

  /**
   * Pack soubor byl vytvořen.
   *
   * @param pack Informace o pack souboru.
   */
  void packCreated(Pack pack);
}
