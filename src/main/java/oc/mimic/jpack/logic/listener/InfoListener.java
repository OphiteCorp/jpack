package oc.mimic.jpack.logic.listener;

import oc.mimic.jpack.struct.PackHeaderFull;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;

/**
 * Listener pro odchycení událostí při zobrazení informací o pack souboru.
 *
 * @author mimic
 */
public interface InfoListener extends SecretKeyListener {

  /**
   * Nastala změna stavu.
   *
   * @param state Aktuální stav.
   */
  void stateChanged(ProcessState state);

  /**
   * Hlavička pack souboru byla otevřena.
   *
   * @param packFile Pack soubor.
   * @param header   Hlavička pack souboru.
   */
  void headerOpened(File packFile, PackHeaderFull header);
}
