package oc.mimic.jpack.logic.listener;

import oc.mimic.jpack.struct.FileProgressReadInfo;
import oc.mimic.jpack.struct.PackHeaderFull;
import oc.mimic.jpack.struct.ReadingPackFileData;
import oc.mimic.jpack.struct.Unpack;
import oc.mimic.jpack.struct.enums.ProcessState;

import java.io.File;
import java.util.Collection;

/**
 * Listener pro odchycení událostí při unpacku souboru.
 *
 * @author mimic
 */
public interface UnpackListener extends SecretKeyListener {

  /**
   * Nastala změna stavu.
   *
   * @param state Aktuální stav.
   */
  void stateChanged(ProcessState state);

  /**
   * Hlavička pack souboru byla otevřena.
   *
   * @param packFile Pack soubor.
   * @param header   Hlavička pack souboru.
   */
  void headerOpened(File packFile, PackHeaderFull header);

  /**
   * Probíhá čtení pack souboru.
   *
   * @param data     Informace o průběhu čtení.
   * @param progress Aktuální průběh. Obsahuje všechny běžící procesy pro více vláken a obsahuje pouze nedokončené.
   *                 Soubor, který byl již kompletně načten z kolekce zmizí. Všechna logika musí být thread-safe.
   */
  void readingPackFile(ReadingPackFileData data, Collection<FileProgressReadInfo> progress);

  /**
   * Rozbalení pack souboru bylo dokončeno.
   *
   * @param unpack Informace o rozbaleném pack souboru.
   */
  void unpackCompleted(Unpack unpack);
}
