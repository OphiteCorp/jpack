package oc.mimic.jpack.logic;

import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.struct.PackHeaderFile;
import oc.mimic.jpack.struct.PackHeaderFull;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Pomocné metody pro výpis informací do konzole.
 *
 * @author mimic
 */
public final class JPackPrinter {

  /**
   * Vypiše všechny soubory z pack hlavičky do stromové struktůry.
   *
   * @param headerFull Hlavička pack souboru.
   */
  public static void printPackHeaderFiles(PackHeaderFull headerFull) {
    List<PackHeaderFile> files = new ArrayList<>(headerFull.getHeader().getFiles());

    // přidá souborům kořen
    for (var file : files) {
      file.setRelativePath(".\\" + file.getRelativePath());
    }
    // vytvoří kořen, od kterého se soubory vypíší
    var root = new PackHeaderFile(Integer.MIN_VALUE);
    root.setDirectory(true);
    root.setRelativePath(".");

    // seřadí vše a vypíše
    files = files.stream().sorted(Comparator.comparing(PackHeaderFile::getRelativePath)).collect(Collectors.toList());
    var alreadyPrinted = new ArrayList<String>();

    printPackHeaderFilesRec(files, root, "  ", true, true, alreadyPrinted);
  }

  /**
   * Převede boolean na text.
   *
   * @param value Vstupní hodnota.
   *
   * @return Textová hodnota.
   */
  public static String toStr(boolean value) {
    return value ? "True" : "False";
  }

  /**
   * Převede velikost bufferu na textovou hodnotu.
   *
   * @param bufferSize Velikost bufferu.
   *
   * @return Textová hodnota velikosti bufferu.
   */
  public static String toBufferSizeStr(long bufferSize) {
    return bufferSize + " (" + FilesizeFormatter.format(bufferSize) + ")";
  }

  /**
   * Převede informaci o vícevláknovém zpracování na text.
   *
   * @param multithreaded Je povoleno vícevláknové zpracování?
   *
   * @return Textová hodnota zpracování.
   */
  public static String toMultithreadedStr(boolean multithreaded) {
    if (multithreaded) {
      var threads = JPackService.MAXIMUM_THREADS;
      return toStr(true) + " (" + threads + " threads)";
    }
    return toStr(false);
  }

  /**
   * Vypíše soubory ve stromové struktůře. Tato metoda se volá rekurzivně.
   *
   * @param files          Seznam souborů v hlavičce pack souboru.
   * @param current        Aktuální soubor.
   * @param prefix         Odsazení.
   * @param isRoot         Je vstupní soubor kořenový?
   * @param lastInPeers    Rozcestí pro adresář a soubor.
   * @param alreadyPrinted Seznam všech souborů, které byly již vypsány. Tyto soubory se přeskočí.
   *                       Tato kolekce odebírá nově vzniklé duplicity kvůli vytvoření podadresářů.
   */
  private static void printPackHeaderFilesRec(List<PackHeaderFile> files, PackHeaderFile current, String prefix,
          boolean isRoot, boolean lastInPeers, List<String> alreadyPrinted) {

    // nastane pouze v případě, že v getChildrenFiles() vznikla duplicita, tak se znovu nevypíše
    if (alreadyPrinted.contains(current.getRelativePath())) {
      return;
    }
    System.out.print(prefix);
    System.out.print(isRoot ? "`-- " : (lastInPeers ? "`-- " : "|-- "));

    var file = new File(current.getRelativePath());
    if (current.isDirectory()) {
      System.out.printf("[%s]", isRoot ? "pack" : file.getName());
      alreadyPrinted.add(current.getRelativePath());
    } else {
      System.out.printf("%s (%s)", file.getName(), FilesizeFormatter.format(current.getFilesize().longValue()));
      alreadyPrinted.add(current.getRelativePath());
    }
    System.out.println();

    // získá všechny potomky souboru; může vytvořit i nové, pokud se nalezne podadresář, který ještě nebyl vytvořen
    // tím vznikne duplicita a je třeba jí řešit pomocí alreadyPrinted
    var children = getChildrenFiles(files, current, isRoot);
    var padding = "    ";

    for (var i = 0; i < children.size() - 1; ++i) {
      var prefixValue = prefix + (lastInPeers ? padding : "|   ");
      printPackHeaderFilesRec(files, children.get(i), prefixValue, false, false, alreadyPrinted);
    }
    if (children.size() > 0) {
      var prefixValue = prefix + (lastInPeers ? padding : "|   ");
      printPackHeaderFilesRec(files, children.get(children.size() - 1), prefixValue, false, true, alreadyPrinted);
    }
  }

  /**
   * Komplikovaná logika pro získání potomků pro konkrétní soubor v hlavičce.
   *
   * @param files   Aktuální soubory v souboru.
   * @param current Aktuální soubor.
   * @param isRoot  Jde o hlavní adresář?
   *
   * @return Seznam souborů v aktuálním souboru.
   */
  private static List<PackHeaderFile> getChildrenFiles(List<PackHeaderFile> files, PackHeaderFile current,
          boolean isRoot) {

    if (current.isDirectory()) {
      var out = new ArrayList<PackHeaderFile>();

      // tento cyklus prvně přidá všechny podadresáře pro vstupní soubor
      for (var it = files.iterator(); it.hasNext(); ) {
        var file = it.next();

        // pokud je soubor adresář, tak ho automaticky přidáme
        if (file.isDirectory()) {
          out.add(file);
          it.remove();
        } else if (!isRoot) {
          // pokud není root a jeho cesta začíná s cestou vstupního souboru
          if (file.getRelativePath().startsWith(current.getRelativePath())) {
            var relative = file.getRelativePath().substring(current.getRelativePath().length() + 1);
            var parts = relative.split("\\\\");

            // pokud bude cest více, tak se soubor nachází v podadresáři a ten je potřeba prvně založit
            // bohužel tím vznikne duplicita záznamu, proto je potřeba tyto duplicity zpětně detekovat v metodě, která
            // tuto metodu volá
            if (parts.length > 1) {
              var path = Paths.get(current.getRelativePath(), parts[0]).toString();
              // vytvoření nového souboru ručně, kterým bude adresář
              var newOne = new PackHeaderFile(Integer.MIN_VALUE);
              newOne.setDirectory(true);
              newOne.setRelativePath(path);
              out.add(newOne);
            }
          }
        }
      }
      // jako druhý krok se přidají všechny soubory pro daný adresář
      for (var file : files) {
        if (file.getRelativePath().startsWith(current.getRelativePath())) {
          var isDir = false;

          // zde je potřeba detekovat, jestli nebyl přidán nový soubor ručně
          for (var f : out) {
            if (f.isDirectory() && file.getRelativePath().startsWith(f.getRelativePath())) {
              isDir = true;
              break;
            }
          }
          if (!isDir) {
            out.add(file);
          }
        }
      }
      return out;
    }
    return Collections.emptyList();
  }
}
