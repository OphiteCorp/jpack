package oc.mimic.jpack.logic.task;

import oc.mimic.jpack.struct.FileProgressReadInfo;

/**
 * Listener pro zachycení událostí při čtení souboru z pack souboru.
 *
 * @author mimic
 */
public interface ReadSingleFileListener {

  /**
   * Aktualizuje průběh čtení souboru. Tato metoda se volá s jiného vlákna. Veškerá logika musí být thread-safe.
   *
   * @param task     Tásk, která zpracovává soubor.
   * @param progress Aktuální průběh.
   */
  void updateProgress(ReadSingleFileTask task, FileProgressReadInfo progress);
}
