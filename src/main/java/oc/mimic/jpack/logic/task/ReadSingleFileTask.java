package oc.mimic.jpack.logic.task;

import oc.mimic.jpack.component.AesGcmCipher;
import oc.mimic.jpack.helper.IOBytesHelper;
import oc.mimic.jpack.helper.ShuffleHelper;
import oc.mimic.jpack.helper.ZipHelper;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.logic.exception.JPackException;
import oc.mimic.jpack.struct.FileProgressReadInfo;
import oc.mimic.jpack.struct.PackHeaderFile;
import oc.mimic.jpack.struct.PackHeaderFull;
import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Tásk pro rozbalení souboru z packu. Tásk je vždy pro jeden soubor.
 *
 * @author mimic
 */
public final class ReadSingleFileTask implements Runnable {

  private final List<ReadSingleFileListener> listeners;
  private final File packFile;
  private final File outputFile;
  private final long entryPointLength;
  private final PackHeaderFull headerFull;
  private final PackHeaderFile headerFile;

  /**
   * Vytvoří instanci tásku pro rozbalení jednoho souboru z packu.
   *
   * @param packFile         Vstupní pack soubor.
   * @param outputFile       Výstupní soubor.
   * @param entryPointLength Velikost souboru v packu.
   * @param headerFull       Hlavička pack souboru.
   * @param headerFile       Aktuální soubor v hlavičce.
   */
  public ReadSingleFileTask(File packFile, File outputFile, long entryPointLength, PackHeaderFull headerFull,
          PackHeaderFile headerFile) {

    this.packFile = packFile;
    this.outputFile = outputFile;
    this.entryPointLength = entryPointLength;
    this.headerFull = headerFull;
    this.headerFile = headerFile;
    listeners = new ArrayList<>(1);
  }

  @Override
  public void run() {
    if (!outputFile.getParentFile().exists()) {
      outputFile.getParentFile().mkdirs();
    }
    try (var raf = new RandomAccessFile(packFile, "r")) {
      raf.seek(headerFile.getEntryPoint()); // nastaví pozici souboru v packu

      try (var rafFile = new RandomAccessFile(outputFile, "rw")) {
        var header = headerFull.getHeader();
        var encryptMode = header.getEncryptModeType();
        var compressMode = header.getCompressModeType();
        var bufferSize = header.getBufferSize().intValue();
        var buffer = new byte[bufferSize * 2]; // je potřeba rezerva
        var left = entryPointLength;
        var totalRead = 0L;
        var read = 0;

        var progress = new FileProgressReadInfo();
        progress.setHeaderFile(headerFile);
        progress.setOutputFile(outputFile);
        progress.setThreadId(Thread.currentThread().getId());

        // soubor je prázdný
        if (left == 0) {
          progress.setPointerPos(totalRead);
          progress.setCurrentRead(0);
          progress.setFinished(true);
          onUpdateProgress(progress);
        } else {
          while (read != -1 && left != 0) {
            var decompressDataLen = 0;
            int readLen;

            // získá délku dat, která se mají z pack souboru přečíst
            if (compressMode != CompressMode.NONE || encryptMode != EncryptMode.HEADER) {
              var readLenData = JPackUtils.readNumber(raf, false);
              readLen = readLenData.getNumber().intValue();
              left -= readLenData.getNumberLength();
              raf.seek(raf.getFilePointer() + readLenData.getNumberLength());

              // pokud bude existovat komprese, tak následující hodnota je velikost původních dat před kompresí
              if (compressMode != CompressMode.NONE) {
                var decompressDataLenData = JPackUtils.readNumber(raf, false);
                decompressDataLen = decompressDataLenData.getNumber().intValue();
                left -= decompressDataLenData.getNumberLength();
                raf.seek(raf.getFilePointer() + decompressDataLenData.getNumberLength());
              }
            } else {
              // pokud nebude existovat komprese ani encrypt
              readLen = (int) Math.min(left, bufferSize);
            }
            // načte data dle zjištěné velikosti
            read = raf.read(buffer, 0, readLen);
            if (read != -1) {
              var data = Arrays.copyOfRange(buffer, 0, read);

              if (header.isShuffle()) {
                if (header.isRot128()) {
                  data = IOBytesHelper.rot128(data);
                }
                ShuffleHelper.unshuffle(data, headerFile.getShuffleSecret());
              }
              if (encryptMode == EncryptMode.ALL) {
                if (header.isRot128()) {
                  data = IOBytesHelper.rot128(data);
                }
                data = AesGcmCipher.decryptToBytes(data, headerFull.getSecretKey());
              }
              if (compressMode != CompressMode.NONE) {
                if (header.isRot128()) {
                  data = IOBytesHelper.rot128(data);
                }
                data = decompressData(data, buffer, decompressDataLen, compressMode);
              }
              if (header.isRot128()) {
                data = IOBytesHelper.rot128(data);
              }
              left -= read;

              if (left < 0) {
                throw new JPackException("An unexpected error occurred while reading the file: " + outputFile);
              }
              totalRead += data.length;
              progress.setPointerPos(totalRead);
              progress.setCurrentRead(data.length);
              progress.setFinished(left == 0L);
              onUpdateProgress(progress);
              rafFile.write(data);
            }
          }
        }
      }
    } catch (Exception e) {
      throw new JPackException("An error occurred while unpacking the '" + outputFile + "' file.", e);
    } finally {
      listeners.clear();
    }
  }

  /**
   * Přidá nový listener.
   *
   * @param listener Instance listeneru.
   */
  public void addListener(ReadSingleFileListener listener) {
    if (listener != null) {
      listeners.add(listener);
    }
  }

  /**
   * Aktualizuje průběh čtení souboru.
   *
   * @param progress Aktuální průběh.
   */
  private synchronized void onUpdateProgress(FileProgressReadInfo progress) {
    for (var listener : listeners) {
      listener.updateProgress(this, progress);
    }
  }

  private static byte[] decompressData(byte[] data, byte[] buffer, int decompressDataLen, CompressMode compressMode)
          throws IOException {

    switch (compressMode) {
      case LZ4:
      case LZ4HC:
        var decompressor = JPackUtils.LZ4_FACTORY.fastDecompressor();
        decompressor.decompress(data, 0, buffer, 0, decompressDataLen);
        return Arrays.copyOfRange(buffer, 0, decompressDataLen);

      case GZIP:
        return ZipHelper.gzipUncompress(data, buffer.length);

      case BZIP2:
        return ZipHelper.bzip2Uncompress(data, buffer.length);

      case XZ:
        return ZipHelper.xzUncompress(data, buffer.length);

      default:
        return data;
    }
  }
}
