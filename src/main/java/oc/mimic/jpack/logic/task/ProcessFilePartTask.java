package oc.mimic.jpack.logic.task;

import oc.mimic.jpack.component.AesGcmCipher;
import oc.mimic.jpack.helper.IOBytesHelper;
import oc.mimic.jpack.helper.ShuffleHelper;
import oc.mimic.jpack.helper.ZipHelper;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.struct.PackConfig;
import oc.mimic.jpack.struct.PackHeader;
import oc.mimic.jpack.struct.PackHeaderFile;
import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.Callable;

/**
 * Tásk pro zpracování části dat pro soubor do packu.
 *
 * @author mimic
 */
public final class ProcessFilePartTask implements Callable<ProcessFilePartResult> {

  private PackConfig config;
  private final PackHeader header;
  private final PackHeaderFile headerFile;
  private final byte[] secretKey;
  private byte[] data;

  /**
   * Vytvoří novou instanci tásku.
   *
   * @param config     Konfigurace pack souboru.
   * @param header     Hlavička pack souboru.
   * @param headerFile Hlavička souboru.
   * @param secretKey  Tajný klíč pro encrypt.
   * @param data       Vstupní data souboru.
   */
  public ProcessFilePartTask(PackConfig config, PackHeader header, PackHeaderFile headerFile, byte[] secretKey,
          byte[] data) {

    this.config = config;
    this.header = header;
    this.headerFile = headerFile;
    this.secretKey = secretKey;
    this.data = data;
  }

  @Override
  public ProcessFilePartResult call() throws Exception {
    var result = new ProcessFilePartResult();

    if (header.isRot128()) {
      data = IOBytesHelper.rot128(data);
    }
    // v případě komprese dat se musí prvně zapsat nová velikost dat, protože se bude lišit a může být i větší,
    // než samotná velikost bufferu
    var compressMode = CompressMode.getByCode(header.getCompressMode());
    if (compressMode != CompressMode.NONE) {
      result.setDecompressDataLength(data.length);
      data = compressData(data, compressMode, config.getCompressLevel());
      result.setDataLength(data.length);

      if (header.isRot128()) {
        data = IOBytesHelper.rot128(data);
      }
    }
    // encrypt dat - opět je potřeba získat novou velikost dat
    if (header.getEncryptModeType() == EncryptMode.ALL) {
      data = AesGcmCipher.encryptToBytes(data, secretKey);
      result.setDataLength(data.length);

      if (header.isRot128()) {
        data = IOBytesHelper.rot128(data);
      }
    }
    // v případě shuffle upraví byty (ponechá délku)
    if (header.isShuffle()) {
      ShuffleHelper.shuffle(data, headerFile.getShuffleSecret());
      if (header.isRot128()) {
        data = IOBytesHelper.rot128(data);
      }
    }
    result.setData(data);
    return result;
  }

  /**
   * Komprimace dat podle módu.
   *
   * @param data          Vstupní data, která se budou komprimovat.
   * @param compressMode  Mód komprese.
   * @param compressLevel Úroveň komprese.
   *
   * @return Komprimované pole bytů.
   */
  private static byte[] compressData(byte[] data, CompressMode compressMode, int compressLevel) throws IOException {
    switch (compressMode) {
      case LZ4:
        var fastCompressor = JPackUtils.LZ4_FACTORY.fastCompressor();
        var fastMaxLength = fastCompressor.maxCompressedLength(data.length);
        var fastBuffer = new byte[fastMaxLength];
        var fastLength = fastCompressor.compress(data, 0, data.length, fastBuffer, 0, fastMaxLength);
        return Arrays.copyOfRange(fastBuffer, 0, fastLength);

      case LZ4HC:
        var hcCompressor = JPackUtils.LZ4_FACTORY.highCompressor(compressLevel);
        var hcMaxLength = hcCompressor.maxCompressedLength(data.length);
        var hcBuffer = new byte[hcMaxLength];
        var hcLength = hcCompressor.compress(data, 0, data.length, hcBuffer, 0, hcMaxLength);
        return Arrays.copyOfRange(hcBuffer, 0, hcLength);

      case GZIP:
        return ZipHelper.gzipCompress(data, compressLevel);

      case BZIP2:
        return ZipHelper.bzip2Compress(data);

      case XZ:
        return ZipHelper.xzCompress(data, compressLevel);

      default:
        return data;
    }
  }
}
