package oc.mimic.jpack.logic.task;

/**
 * Tásk pro zpracování dat pro zápis do pack souboru.
 *
 * @author mimic
 */
public final class ProcessFilePartResult {

  private byte[] data; // data po zpracování
  private int decompressDataLength = Integer.MIN_VALUE; // délka dat před kompresí
  private int dataLength = Integer.MIN_VALUE; // nová délka dat

  /**
   * Je délka dat nastavena?
   *
   * @return Délka dat.
   */
  public boolean isDataLengthSet() {
    return (dataLength != Integer.MIN_VALUE);
  }

  /**
   * Je délka dekomprimovaných dat nastavena?
   *
   * @return Délka dekomprimovaných dat.
   */
  public boolean isDecompressDataLengthSet() {
    return (decompressDataLength != Integer.MIN_VALUE);
  }

  public int getDecompressDataLength() {
    return decompressDataLength;
  }

  public void setDecompressDataLength(int decompressDataLength) {
    this.decompressDataLength = decompressDataLength;
  }

  public byte[] getData() {
    return data;
  }

  public void setData(byte[] data) {
    this.data = data;
  }

  public int getDataLength() {
    return dataLength;
  }

  public void setDataLength(int dataLength) {
    this.dataLength = dataLength;
  }
}
