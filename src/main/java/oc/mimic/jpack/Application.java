package oc.mimic.jpack;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import oc.mimic.jpack.logic.handler.JPackConsolePackHandler;
import oc.mimic.jpack.logic.handler.JPackConsolePackHeaderHandler;
import oc.mimic.jpack.logic.handler.JPackConsoleUnpackHandler;
import oc.mimic.jpack.params.Params;
import oc.mimic.jpack.params.data.Mode;
import oc.mimic.jpack.struct.PackConfig;
import oc.mimic.jpack.struct.UnpackConfig;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Paths;

/**
 * Aplikace jPack.
 *
 * @author mimic
 */
public final class Application {

  /**
   * hlavní main() metoda.
   *
   * @param args Parametry aplikace. Některé jsou povinné.
   */
  public static void main(String[] args) {
    var params = new Params();
    var cmd = JCommander.newBuilder().addObject(params).build();

    if (args.length > 0) {
      try {
        cmd.parse(args);
        specificValidations(params);

      } catch (ParameterException e) {
        System.out.println("Invalid input parameters:");
        System.out.println(e.getMessage());
        return;
      }
      if (params.isHelp()) {
        printInfo(cmd);
        return;
      }
    } else {
      printInfo(cmd);
      return;
    }
    if (params.isSilent()) {
      silentMode();
    }
    // vypíše logo aplikace
    char logo[] = { ' ', ' ', ' ', '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '_', ' ', ' ', '_', '_', '_',
            '_', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '_', '_', ' ', ' ',
            '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '(', '_', ')', '/', ' ', '_', '_', ' ', '\\', ' ', '_',
            '_', '_', '_', ' ', '_', ' ', '_', '_', '_', '_', '_', ' ', '/', ' ', '/', '_', '_', '\n', ' ', ' ', ' ',
            ' ', ' ', ' ', ' ', ' ', '/', ' ', '/', '/', ' ', '/', '_', '/', ' ', '/', '/', ' ', '_', '_', ' ', '`',
            '/', '/', ' ', '_', '_', '_', '/', '/', ' ', '/', '/', '_', '/', '\n', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            '/', ' ', '/', '/', ' ', '_', '_', '_', '_', '/', '/', ' ', '/', '_', '/', ' ', '/', '/', ' ', '/', '_',
            '_', ' ', '/', ' ', ',', '<', ' ', ' ', ' ', '\n', ' ', ' ', ' ', ' ', '_', '_', '/', ' ', '/', '/', '_',
            '/', ' ', ' ', ' ', ' ', ' ', '\\', '_', '_', ',', '_', '/', ' ', '\\', '_', '_', '_', '/', '/', '_', '/',
            '|', '_', '|', ' ', ' ', '\n', ' ', ' ', ' ', '/', '_', '_', '_', '/', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
            ' ' };
    System.out.println(new String(logo) + "\n");

    // zpracuje vybraný mód
    switch (params.getMode()) {
      case PACK:
        var packConfig = new PackConfig();
        packConfig.setBufferSize(params.getBufferSize());
        packConfig.setSecretKey(params.getSecretkey());
        packConfig.setCompressMode(params.getCompressMode());
        packConfig.setEncryptMode(params.getEncryptMode());
        packConfig.setCompressLevel(params.getCompressLevel());
        packConfig.setRot128(params.isRot128());
        packConfig.setShuffle(params.isShuffle());
        packConfig.setPasswordless(!params.isPasswordRequired());
        packConfig.setMultithreaded(params.isMultithreaded());
        packConfig.setDeleteDirectoryAfterFinish(params.isDeleteSource());

        new JPackConsolePackHandler().pack(packConfig, params.getInput(), params.getOutput());
        break;

      case UNPACK:
        var unpackConfig = new UnpackConfig();
        unpackConfig.setMultithreaded(params.isMultithreaded());
        unpackConfig.setDeletePackAfterFinish(params.isDeleteSource());
        unpackConfig.setSecretKey(params.getSecretkey());

        new JPackConsoleUnpackHandler().unpack(unpackConfig, params.getInput(), params.getOutput());
        break;

      case INFO:
        new JPackConsolePackHeaderHandler().print(params.getInput(), params.isDetail(), params.getSecretkey());
        break;
    }
  }

  private static void printInfo(JCommander cmd) {
    System.out.println(
            "Welcome to JPack, which allows you to create an archive for the entire directory with the strongest data encryption support.");
    System.out.println("This application is not designed for high data compression, but emphasizes high security.");
    System.out.println(
            "Supports multi-threaded processing. The speed of creating and unpacking the pack file is guaranteed.");
    System.out.println();
    cmd.usage();
  }

  private static void silentMode() {
    System.setOut(new PrintStream(new OutputStream() {
      @Override
      public void write(int b) {
      }
    }));
  }

  private static void specificValidations(Params params) {
    if (params.getMode() == Mode.PACK) {
      if (params.getOutput() == null) {
        throw new ParameterException("The input parameter " + Params.OUTPUT + " is mandatory for this mode.");
      }
    }
    if (params.getMode() == Mode.UNPACK) {
      if (params.getOutput() == null) {
        var name = new File(params.getInput()).getAbsolutePath();
        name = name.replaceFirst("[.][^.]+$", "");
        params.setOutput(Paths.get("").toAbsolutePath() + "\\" + name);
      }
    }
  }
}
