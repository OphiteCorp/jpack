package oc.mimic.jpack.component;

import java.text.NumberFormat;

/**
 * Pomocná třída pro formátování velikosti souboru.<br>
 * Optimalizováno pro vysokou rychlost formátování.
 *
 * @author mimic
 */
public final class FilesizeFormatter {

  private static final NumberFormat FORMAT;
  private static final long KB = 1024;
  private static final long MB = KB * 1024;
  private static final long GB = MB * 1024;
  private static final long TB = GB * 1024;
  private static final long PB = TB * 1024;
  private static final long EB = PB * 1024;

  private static int fractionDigits = 2;

  static {
    FORMAT = NumberFormat.getNumberInstance();
    FORMAT.setMaximumFractionDigits(fractionDigits); // počet desetinných míst
  }

  /**
   * Počet desetinných míst použitích při formátování.
   *
   * @return Počet desetinných míst.
   */
  public static int getFractionDigits() {
    return fractionDigits;
  }

  /**
   * Nastaví počet desetinných míst použitích při formátování.
   *
   * @param fractionDigits Počet desetinných míst.
   */
  public static void setFractionDigits(int fractionDigits) {
    FilesizeFormatter.fractionDigits = fractionDigits;
  }

  /**
   * Naformátuje velikost souboru na čitelný formát.
   *
   * @param bytes Velikost souboru v bytech.
   *
   * @return Čitelný formát.
   */
  public static String format(long bytes) {
    bytes = Math.abs(bytes);

    if (bytes >= 0 && bytes < KB) {
      return bytes + " B";

    } else if (bytes >= KB && bytes < MB) {
      return FORMAT.format((bytes / (double) KB)) + " KB";

    } else if (bytes >= MB && bytes < GB) {
      return FORMAT.format((bytes / (double) MB)) + " MB";

    } else if (bytes >= GB && bytes < TB) {
      return FORMAT.format((bytes / (double) GB)) + " GB";

    } else if (bytes >= TB && bytes < PB) {
      return FORMAT.format((bytes / (double) TB)) + " TB";

    } else if (bytes >= PB && bytes < EB) {
      return FORMAT.format((bytes / (double) PB)) + " PB";

    } else if (bytes >= EB) {
      return FORMAT.format((bytes / (double) EB)) + " EB";

    } else {
      return bytes + " Bytes";
    }
  }
}
