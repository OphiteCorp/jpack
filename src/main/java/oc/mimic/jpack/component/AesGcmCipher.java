package oc.mimic.jpack.component;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Pomocná třída pro práci s AES-GCM s 256bit klíčem. Obsahuje encrypt/decrypt a nějaké pomocné metody kolem AES.<br>
 * Všechno je nastaveno na maximální bezpečnost.
 *
 * @author mimic
 */
public final class AesGcmCipher {

  private static final String CIPHER = "AES/GCM/NoPadding";
  private static final String ALGORITHM = "AES";

  // používá se pro výpočet počtu iterací hesla, aby se každý rok zvedla složitost; tento rok neměnit
  private static final int AES_KEY_START_YEAR = 2019;
  private static final int AES_KEY_SIZE = 256; // maximum pro AES
  public static final int AES_SECRET_KEY_LENGTH = AES_KEY_SIZE / 8; // délka tajného klíče
  public static final int GCM_IV_LENGTH = 12; // nejlepší pro GCM (mimo GCM bývá 16)
  private static final int GCM_TAG_LENGTH = 128; // maximum pro GCM

  /**
   * Vygeneruje nový náhodný tajný klíč.
   *
   * @return Tajný klíč. Délka je 32 bytů (256/8).
   */
  public static byte[] generateSecretKey() {
    try {
      var generator = KeyGenerator.getInstance(ALGORITHM);
      generator.init(AES_KEY_SIZE);
      return generator.generateKey().getEncoded();

    } catch (Exception e) {
      throw new RuntimeException("An error occurred while generating a random secret key.", e);
    }
  }

  /**
   * Vygeneruje nový náhodný tajný klíč.
   *
   * @param password Vlastní heslo.
   *
   * @return Tajný klíč. Délka je 32 bytů (256/8).
   */
  public static byte[] generateSecretKey(String password) {
    if (password == null || password.length() == 0) {
      throw new IllegalArgumentException("The input password is null or empty.");
    }
    try {
      var factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
      var salt = generateSalt(16); // zde může být 16 - nemá nic společného s GCM
      var year = Calendar.getInstance().get(Calendar.YEAR);
      var yearDiff = (year - AES_KEY_START_YEAR);
      // počet iterací bude v základu 550000 a každý rok se zvedne o min. 50000
      // pro rok 2019 bylo doporučených 600000, což se dodrží i lehce přesáhne
      var iterations = 550000 + ((year * (25 + yearDiff)) * (yearDiff + 1));
      var spec = new PBEKeySpec(password.toCharArray(), salt, iterations, AES_KEY_SIZE);
      return factory.generateSecret(spec).getEncoded();

    } catch (Exception e) {
      throw new RuntimeException("An error occurred while generating a random secret key.", e);
    }
  }

  /**
   * Vytvoří novou instanci cipher pro encrypt.
   *
   * @param secretKey Tajný klíč.
   *
   * @return Nová instance cipher.
   */
  public static Cipher getEncryptCipher(byte[] secretKey)
          throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException,
          NoSuchAlgorithmException {

    var cipher = Cipher.getInstance(CIPHER);
    var secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM);
    var iv = generateGcmIv();
    var gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);

    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);
    return cipher;
  }

  /**
   * Vytvoří novou instanci cipher pro decrypt.
   *
   * @param secretKey Tajný klíč.
   * @param iv        IV data, která byly použity při encryptu.
   *
   * @return Nová instance cipher.
   */
  public static Cipher getDecryptCipher(byte[] secretKey, byte[] iv)
          throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException,
          NoSuchAlgorithmException {

    var cipher = Cipher.getInstance(CIPHER);
    var secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM);
    var gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);

    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);
    return cipher;
  }

  /**
   * Encryptuje vstupní data.
   *
   * @param bytes     Vstupní data pro encrypt.
   * @param secretKey Tajný klíč. Musí být 32 bytů dlouhý!
   *
   * @return Encryptovaná data.
   */
  public static AesResult encrypt(byte[] bytes, byte[] secretKey) {
    try {
      var cipher = Cipher.getInstance(CIPHER);
      var secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM);
      var iv = generateGcmIv();
      var gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);

      cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, gcmParameterSpec);

      var encrypted = cipher.doFinal(bytes);
      var merge = mergeEncryptWithIv(encrypted, iv);

      return new AesResult(merge, secretKey);

    } catch (Exception e) {
      throw new RuntimeException("Data encryption error occurred.", e);
    }
  }

  /**
   * Encryptuje vstupní data.
   *
   * @param bytes        Vstupní data pro encrypt.
   * @param hexSecretKey Tajný klíč v podobě HEX.
   *
   * @return Encryptovaná data.
   */
  public static AesResult encrypt(byte[] bytes, String hexSecretKey) {
    var secretKey = toHexBytes(hexSecretKey);
    return encrypt(bytes, secretKey);
  }

  /**
   * Encryptuje vstupní data.
   *
   * @param bytes Vstupní data pro encrypt.
   *
   * @return Encryptovaná data.
   */
  public static AesResult encrypt(byte[] bytes) {
    return encrypt(bytes, generateSecretKey());
  }

  /**
   * Encryptuje vstupní data.
   *
   * @param bytes     Vstupní data pro encrypt.
   * @param secretKey Tajný klíč. Musí být 32 bytů dlouhý!
   *
   * @return Encryptovaná data.
   */
  public static byte[] encryptToBytes(byte[] bytes, byte[] secretKey) {
    var result = encrypt(bytes, secretKey);
    return result.getResult();
  }

  /**
   * Decryptuje encryptovaná data.
   *
   * @param encryptedBytes Encryptovaná data.
   * @param secretKey      Tajný klíč. Musí být 32 bytů dlouhý!
   *
   * @return Decryptovaná data.
   */
  public static AesResult decrypt(byte[] encryptedBytes, byte[] secretKey) {
    try {
      var cipher = Cipher.getInstance(CIPHER);
      var secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM);
      var iv = readIv(encryptedBytes);
      var encryptedWithoutIv = Arrays.copyOfRange(encryptedBytes, 1 + iv.length, encryptedBytes.length);
      var gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH, iv);

      cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, gcmParameterSpec);
      var decrypted = cipher.doFinal(encryptedWithoutIv);

      return new AesResult(decrypted, secretKey);

    } catch (Exception e) {
      throw new RuntimeException("Data decryption error occurred.", e);
    }
  }

  /**
   * Decryptuje encryptované pole bytů.
   *
   * @param encryptedBytes Encryptovaná data.
   * @param secretKey      Tajný klíč. Musí být 32 bytů dlouhý!
   *
   * @return Decryptovaná data.
   */
  public static byte[] decryptToBytes(byte[] encryptedBytes, byte[] secretKey) {
    var result = decrypt(encryptedBytes, secretKey);
    return result.getResult();
  }

  /**
   * Decryptuje encryptované pole bytů.
   *
   * @param encryptedBytes Encryptovaná data.
   * @param hexSecretKey   Tajný klíč v podobě HEX.
   *
   * @return Decryptovaná data.
   */
  public static byte[] decryptToBytes(byte[] encryptedBytes, String hexSecretKey) {
    var secretKey = toHexBytes(hexSecretKey);
    var result = decrypt(encryptedBytes, secretKey);
    return result.getResult();
  }

  /**
   * Převede data do HEX řetězce.
   *
   * @param bytes Vstupní data.
   *
   * @return HEX řetězec.
   */
  public static String toHex(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if ((bytes.length % 2) != 0) {
      throw new IllegalArgumentException("Input bytes length (" + bytes.length + ") is not valid.");
    }
    var sb = new StringBuilder();
    for (var b : bytes) {
      sb.append(String.format("%02x", b));
    }
    return sb.toString();
  }

  /**
   * Převede HEX řetězec do pole bytes.
   *
   * @param hex Vstupní HEX.
   *
   * @return Bytes.
   */
  public static byte[] toHexBytes(String hex) {
    if (hex == null) {
      throw new IllegalArgumentException("The HEX input string is null.");
    }
    var length = hex.length();
    if ((length % 2) != 0) {
      throw new IllegalArgumentException("The input HEX string has an invalid number of characters (" + length + ").");
    }
    var data = new byte[length / 2];

    for (var i = 0; i < length; i += 2) {
      data[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i + 1), 16));
    }
    return data;
  }

  /**
   * Vygeneruje náhodnou sůl (náhodné bytes). Většinou se používá 8, 12 nebo 16 bytes.
   *
   * @return Náhodné bytes.
   */
  public static byte[] generateSalt(int length) {
    if (length <= 0) {
      throw new IllegalArgumentException("The length of the generated salt is too small.");
    }
    try {
      var salt = new byte[length];
      var random = SecureRandom.getInstance("SHA1PRNG");
      random.nextBytes(salt);
      return salt;

    } catch (Exception e) {
      throw new RuntimeException("An error occurred while generating random salt with length: " + length, e);
    }
  }

  /**
   * Vygeneruje náhodné IV jako sůl pro AES-GCM.
   *
   * @return Náhodné IV.
   */
  private static byte[] generateGcmIv() {
    return generateSalt(GCM_IV_LENGTH);
  }

  /**
   * Získá IV z encryptovaných dat.
   *
   * @param encryptedBytes Encryptovaná data.
   *
   * @return Načtené IV pro daný encrypt.
   */
  private static byte[] readIv(byte[] encryptedBytes) {
    var ivLength = (int) encryptedBytes[0];
    var iv = new byte[ivLength];

    System.arraycopy(encryptedBytes, 1, iv, 0, ivLength);

    return iv;
  }

  /**
   * Vytvoří merge pro encryptovaná data a IV.
   *
   * @param encrypted Encryptovaná data.
   * @param iv        IV, které bylo použito pro encrypt.
   *
   * @return Výstupní merge. Obsahuje na první místě délku IV, poté samotné IV a na konec encryptovaná data.
   */
  private static byte[] mergeEncryptWithIv(byte[] encrypted, byte[] iv) {
    var output = new byte[1 + iv.length + encrypted.length];

    System.arraycopy(new byte[]{ (byte) iv.length }, 0, output, 0, 1);
    System.arraycopy(iv, 0, output, 1, iv.length);
    System.arraycopy(encrypted, 0, output, iv.length + 1, encrypted.length);

    return output;
  }

  /**
   * Obsahuje výsledek encryptu/decryptu.
   */
  public static final class AesResult {

    private final byte[] result;
    private final byte[] secretKey;

    /**
     * Vytvoří instanci s výsledkem.
     *
     * @param result    Výsledek.
     * @param secretKey Tajný klíč.
     */
    private AesResult(byte[] result, byte[] secretKey) {
      this.result = result;
      this.secretKey = secretKey;
    }

    /**
     * Získá tajný klíč v podobě HEX.
     *
     * @return Tajný klíč.
     */
    public String getSecretKeyHex() {
      return toHex(secretKey);
    }

    /**
     * Získá výsledek po encrypt/decrypt.
     *
     * @return Výsledek.
     */
    public byte[] getResult() {
      return result;
    }

    /**
     * Získá tajný klíč.
     *
     * @return Tajný klíč.
     */
    public byte[] getSecretKey() {
      return secretKey;
    }
  }
}
