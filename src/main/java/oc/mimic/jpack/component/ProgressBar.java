package oc.mimic.jpack.component;

/**
 * Konzolový progress bar pro zobrazení průběhu.
 *
 * @author mimic
 */
public final class ProgressBar {

  private static final char[] ANIM_CHARS = { '|', '/', '-', '\\' };
  private static final String FORMAT = "\r[%c] %6.2f%% | %s [%s] %s";
  private static final char PROGRESS_CHAR = '=';
  private static final char PROGRESS_HEAD = '>';
  private static final int PROGRESS_LENGTH = 50;
  private static final String DEFAULT_DONE_MSG = "Done!";
  private static final String DEFAULT_FAIL_MSG = "Failed!";

  private StringBuilder progress;
  private int index;
  private long lastDone;
  private float lastParcent;
  private char lastAnimChar;
  private boolean failed;
  private boolean ready;
  private String failedMessage;
  private int animFrame;

  /**
   * Vytvoří novou instanci.
   */
  public ProgressBar() {
    init();
  }

  /**
   * Zahájí progress. Od této chvíle není možné zapisovat do konzole dokud se progress bar nedokončí. Jinak se volá
   * při každé změně stavu.
   *
   * @param total Výsledný stav.
   */
  public void start(long total) {
    update(0, total, "");
  }

  /**
   * Aktualizuje progress. Od této chvíle není možné zapisovat do konzole dokud se progress bar nedokončí. Jinak se
   * volá při každé změně stavu.
   *
   * @param done  Aktuální stav.
   * @param total Výsledný stav.
   */
  public void update(long done, long total) {
    update(done, total, "");
  }

  /**
   * Aktualizuje progress. Od této chvíle není možné zapisovat do konzole dokud se progress bar nedokončí. Jinak se
   * volá při každé změně stavu.
   *
   * @param done         Aktuální stav.
   * @param total        Výsledný stav.
   * @param progressName Název progresu (např. "Uploading...", "Downloading...", "Processing..." atd.).
   */
  public void update(long done, long total, String progressName) {
    ready = false;

    if (progressName == null) {
      progressName = "";
    }
    var doneValue = fixValue(done);
    var totalValue = fixValue(total);

    if (doneValue > totalValue) {
      doneValue = totalValue - 1;
    }
    var percent = (float) (doneValue * 100) / totalValue;
    var extrachars = ((int) (percent / (100. / PROGRESS_LENGTH))) - index;

    while (extrachars-- > 0) {
      if (index > 0) {
        progress.setCharAt(index - 1, PROGRESS_CHAR);
      }
      if (index < PROGRESS_LENGTH) {
        progress.setCharAt(index++, PROGRESS_HEAD);
      }
    }
    if ((int) lastParcent != (int) percent) {
      animFrame = (animFrame + 1 >= ANIM_CHARS.length) ? 0 : animFrame + 1;
      lastAnimChar = ANIM_CHARS[animFrame];
    }
    var range = calculateRangeProgress(doneValue, totalValue);
    System.out.printf(FORMAT, lastAnimChar, percent, range, progress, progressName);

    lastDone = done;
    lastParcent = percent;
  }

  /**
   * Dokončí progress bar. Automaticky uzavře progress bar, takže je možné vytvořit nový nebo pokračovat v zápisu
   * do konzole (což nejde, pokud je progress bar aktivní).
   *
   * @param total         Výsledný stav.
   * @param prevMsgLength Délka původní zprávy, která se zobrazila při procesu. Pokud žádná nebyla, stačí nastavit 0.
   * @param doneMessage   Zpráva, která se vypíše za progress barem.
   */
  public void done(long total, int prevMsgLength, String doneMessage) {
    if (ready) {
      return;
    }
    if (doneMessage == null) {
      doneMessage = "";
    }
    if (prevMsgLength < 0) {
      prevMsgLength = 0;
    }
    var totalValue = fixValue(total);
    var sb = new StringBuilder(" ".repeat(prevMsgLength));
    sb.insert(0, failed ? failedMessage : doneMessage);

    if (failed) {
      var progress = new StringBuilder(" ".repeat(PROGRESS_LENGTH));
      if (index > 0) {
        progress.replace(0, index - 1, String.valueOf(PROGRESS_CHAR).repeat(index - 1));
        progress.replace(index - 1, index, String.valueOf(PROGRESS_HEAD));
      } else {
        progress.replace(index, index + 1, String.valueOf(PROGRESS_HEAD));
      }
      var range = calculateRangeProgress(lastDone, totalValue);
      System.out.printf(FORMAT, '-', lastParcent, range, progress, sb);
    } else {
      var progress = String.valueOf(PROGRESS_CHAR).repeat(PROGRESS_LENGTH);
      var range = calculateRangeProgress(totalValue, totalValue);
      System.out.printf(FORMAT, 'X', 100f, range, progress, sb);
    }
    System.out.flush();
    System.out.println();
    init();
  }

  /**
   * Dokončí progress bar. Automaticky uzavře progress bar, takže je možné vytvořit nový nebo pokračovat v zápisu
   * do konzole (což nejde, pokud je progress bar aktivní).
   *
   * @param total         Výsledný stav.
   * @param prevMsgLength Délka původní zprávy, která se zobrazila při procesu. Pokud žádná nebyla, stačí nastavit 0.
   */
  public void done(long total, int prevMsgLength) {
    done(total, prevMsgLength, DEFAULT_DONE_MSG);
  }

  /**
   * Dokončí progress bar. Automaticky uzavře progress bar, takže je možné vytvořit nový nebo pokračovat v zápisu
   * do konzole (což nejde, pokud je progress bar aktivní).
   *
   * @param total Výsledný stav.
   */
  public void done(long total) {
    done(total, 0);
  }

  /**
   * Nastaví příznak, že proces nemůže být dokončen, protože něco selhalo.
   *
   * @param failMessage Zpráva, která se má vypsat za progress bar.
   */
  public void fail(String failMessage) {
    if (failedMessage == null) {
      failedMessage = "";
    }
    failed = true;
    failedMessage = failMessage;
  }

  /**
   * Nastaví příznak, že proces nemůže být dokončen, protože něco selhalo.
   */
  public void fail() {
    fail(DEFAULT_FAIL_MSG);
  }

  /**
   * Vše vyresetuje do původního nastavení.
   */
  private void init() {
    progress = new StringBuilder(" ".repeat(PROGRESS_LENGTH));
    index = 0;
    lastDone = 0;
    lastParcent = 0;
    animFrame = 0;
    lastAnimChar = ANIM_CHARS[animFrame];
    ready = true;
    failed = false;
    failedMessage = null;
  }

  /**
   * Vypočte průběh.
   *
   * @param done  Hotovo.
   * @param total Celkový počet.
   *
   * @return Průběh.
   */
  private static String calculateRangeProgress(long done, long total) {
    var doneValue = done;
    if (doneValue < 0) {
      doneValue = total;
    }
    var lengthPlaceholder = "%" + String.valueOf(total).length() + "d";
    return String.format(lengthPlaceholder + "/" + lengthPlaceholder, doneValue, total);
  }

  /**
   * Opraví hodnotu, aby nikdy nebyla menší než 0.
   *
   * @param value Vstupní hodnota.
   *
   * @return Upravená hodnota.
   */
  private static long fixValue(long value) {
    return (value < 0) ? 0 : value;
  }
}
