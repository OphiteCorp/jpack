package oc.mimic.jpack.component;

import java.math.RoundingMode;
import java.text.NumberFormat;

/**
 * Pomocná třída pro formátování procent.
 *
 * @author mimic
 */
public final class PercentFormatter {

  private static final NumberFormat PERCENT_FORMAT;

  static {
    PERCENT_FORMAT = NumberFormat.getPercentInstance();
    PERCENT_FORMAT.setMinimumFractionDigits(2);
    PERCENT_FORMAT.setMaximumFractionDigits(2);
    PERCENT_FORMAT.setRoundingMode(RoundingMode.FLOOR);
  }

  /**
   * Naformátuje vstupní číslo na procento.
   *
   * @param value Vstupní číslo.
   *
   * @return Číslo jako procento.
   */
  public static String format(double value) {
    return PERCENT_FORMAT.format(value / 100.);
  }
}
