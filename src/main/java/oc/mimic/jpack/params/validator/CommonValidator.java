package oc.mimic.jpack.params.validator;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;
import oc.mimic.jpack.component.FilesizeFormatter;
import oc.mimic.jpack.logic.JPackUtils;
import oc.mimic.jpack.params.Params;
import oc.mimic.jpack.params.data.Mode;
import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

/**
 * Běžný validátor pro vstupní parametry.
 *
 * @author mimic
 */
public final class CommonValidator implements IParameterValidator {

  private static final int BUFFER_SIZE_MIN = 1024; // 1 KB
  private static final int BUFFER_SIZE_MAX = 1024 * 1024 * 64; // 64 MB

  @Override
  public void validate(String name, String value) throws ParameterException {
    String msg = null;

    switch (name) {
      case Params.MODE:
        if (!isValidMode(value)) {
          msg = "The application mode '" + value + "' is not valid.";
        }
        break;

      case Params.COMPRESS:
        if (!isValidCompressMode(value)) {
          msg = "The compress mode '" + value + "' is not valid.";
        }
        break;

      case Params.ENCRYPT:
        if (!isValidEncryptMode(value)) {
          msg = "The encrypt mode '" + value + "' is not valid.";
        }
        break;

      case Params.SECRET_KEY:
        if (!isValidSecretKey(value)) {
          msg = "The secret key '" + value + "' is not valid.";
        }
        break;

      case Params.COMPRESS_LEVEL:
        if (!isValidCompressLevel(value)) {
          msg = "The compression level '" + value + "' is not valid. The range must be between [0-9].";
        }
        break;

      case Params.BUFFER_SIZE:
        if (!isValidBufferSize(value)) {
          var minStr = FilesizeFormatter.format(BUFFER_SIZE_MIN);
          var maxStr = FilesizeFormatter.format(BUFFER_SIZE_MAX);
          msg = "Invalid buffer size. The range is: [" + BUFFER_SIZE_MIN + "-" + BUFFER_SIZE_MAX + "] (" + minStr +
                " to " + maxStr + ")";
        }
        break;
    }
    if (msg != null) {
      throw new ParameterException(msg);
    }
  }

  private static boolean isValidMode(String value) {
    return (Mode.getByAlias(value) != null);
  }

  private static boolean isValidCompressMode(String value) {
    return (CompressMode.getByAlias(value) != null);
  }

  private static boolean isValidEncryptMode(String value) {
    return (EncryptMode.getByAlias(value) != null);
  }

  private static boolean isValidSecretKey(String value) {
    try {
      JPackUtils.checkSecretKey(value);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidCompressLevel(String value) {
    try {
      var min = CompressMode.minCompressLevel();
      var max = CompressMode.maxCompressLevel();
      var level = Integer.parseInt(value);
      return (level >= min && level <= max);

    } catch (Exception e) {
      return false;
    }
  }

  private static boolean isValidBufferSize(String value) {
    try {
      var size = Integer.valueOf(value);
      return (size >= BUFFER_SIZE_MIN && size <= BUFFER_SIZE_MAX);
    } catch (Exception e) {
      return false;
    }
  }
}
