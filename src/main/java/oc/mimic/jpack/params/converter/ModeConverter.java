package oc.mimic.jpack.params.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.jpack.params.data.Mode;

/**
 * Converter pro parametr módu.
 *
 * @author mimic
 */
public final class ModeConverter implements IStringConverter<Mode> {

  @Override
  public Mode convert(String value) {
    return Mode.getByAlias(value.toUpperCase());
  }
}
