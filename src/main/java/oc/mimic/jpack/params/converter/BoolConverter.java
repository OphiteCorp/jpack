package oc.mimic.jpack.params.converter;

import com.beust.jcommander.converters.BaseConverter;

import java.util.Arrays;
import java.util.List;

/**
 * Converter pro parametr typu boolean.
 *
 * @author mimic
 */
public final class BoolConverter extends BaseConverter<Boolean> {

  private static final List<String> TRUE_ALIASES = Arrays.asList("1", "true", "yes", "y");

  public BoolConverter(String optionName) {
    super(optionName);
  }

  @Override
  public Boolean convert(String value) {
    return TRUE_ALIASES.contains(value.toLowerCase());
  }
}
