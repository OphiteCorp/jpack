package oc.mimic.jpack.params.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.jpack.struct.enums.EncryptMode;

/**
 * Converter pro parametr módu encryptu.
 *
 * @author mimic
 */
public final class EncryptModeConverter implements IStringConverter<EncryptMode> {

  @Override
  public EncryptMode convert(String value) {
    return EncryptMode.getByAlias(value);
  }
}
