package oc.mimic.jpack.params.converter;

import com.beust.jcommander.IStringConverter;
import oc.mimic.jpack.struct.enums.CompressMode;

/**
 * Converter pro parametr módu komprese.
 *
 * @author mimic
 */
public final class CompressModeConverter implements IStringConverter<CompressMode> {

  @Override
  public CompressMode convert(String value) {
    return CompressMode.getByAlias(value.toUpperCase());
  }
}
