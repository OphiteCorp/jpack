package oc.mimic.jpack.params;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.converters.StringConverter;
import oc.mimic.jpack.params.converter.BoolConverter;
import oc.mimic.jpack.params.converter.CompressModeConverter;
import oc.mimic.jpack.params.converter.EncryptModeConverter;
import oc.mimic.jpack.params.converter.ModeConverter;
import oc.mimic.jpack.params.data.Mode;
import oc.mimic.jpack.params.validator.CommonValidator;
import oc.mimic.jpack.struct.PackConfig;
import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Parametry aplikace.
 *
 * @author mimic
 */
@Parameters(commandDescription = "Available application parameters", separators = ":")
public final class Params {

  public static final String MODE = "-m";
  public static final String INPUT = "-in";
  public static final String OUTPUT = "-out";

  public static final String SECRET_KEY = "-key";
  public static final String MULTITHREADED = "-mt";
  public static final String COMPRESS = "-c";
  public static final String COMPRESS_LEVEL = "-cl";
  public static final String ENCRYPT = "-e";
  public static final String ROT128 = "-r";
  public static final String SHUFFLE = "-s";
  public static final String PASSWORD_REQ = "-pass";
  public static final String BUFFER_SIZE = "-bs";
  public static final String DELETE_SOURCE = "-purge";
  public static final String DETAIL = "-detail";
  public static final String SILENT = "-silent";

  public static final String HELP = "--help";

  @Parameter
  private List<String> parameters = new ArrayList<>(); // obsahuje všechny ostatní parametry, které nejsou podporované

  @Parameter(names = MODE,
             order = 1,
             required = true,
             validateWith = CommonValidator.class,
             converter = ModeConverter.class,
             description = "Application mode.")
  private Mode mode;

  @Parameter(names = INPUT,
             order = 10,
             required = true,
             validateWith = CommonValidator.class,
             converter = StringConverter.class,
             description = "Input. To create a pack file, it's a directory. When you unpack a pack file or get information, it will be the path to the pack file.")
  private String input;

  @Parameter(names = OUTPUT,
             order = 11,
             validateWith = CommonValidator.class,
             converter = StringConverter.class,
             description = "Output. When you create a pack file, it is the directory from which to create the file. In case of unpacking or getting information, there will be a path to the pack file. If the parameter is not specified, the current directory is used (only for unpack).")
  private String output;

  @Parameter(names = SECRET_KEY,
             order = 20,
             validateWith = CommonValidator.class,
             converter = StringConverter.class,
             description = "It will be used to create a pack file. If not specified, random is generated. It is also required when working with a pack file that is locked with a key.")
  private String secretkey = PackConfig.DEFAULT_SECRET_KEY;

  @Parameter(names = MULTITHREADED,
             order = 21,
             arity = 1,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "Uses multithreaded processing. I recommend always leave it on.")
  private boolean multithreaded = PackConfig.DEFAULT_MULTITHREADED;

  @Parameter(names = COMPRESS,
             order = 22,
             validateWith = CommonValidator.class,
             converter = CompressModeConverter.class,
             description = "The mode of compression pack file. The order of all types is based on compression speed and quality. Generally I recommend GZIP, BZIP2, or LZ4. There is no point not to use. The fastest LZ4 is almost comparable to no compression.")
  private CompressMode compressMode = PackConfig.DEFAULT_COMPRESS_MODE;

  @Parameter(names = COMPRESS_LEVEL,
             order = 23,
             validateWith = CommonValidator.class,
             converter = IntegerConverter.class,
             description = "Compression level. The level can only be set for GZIP, XZ types where is range [0-9] and for LZ4HC is [1-17 (9 is the best)].")
  private int compressLevel = PackConfig.DEFAULT_COMPRESS_LEVEL;

  @Parameter(names = ENCRYPT,
             order = 24,
             validateWith = CommonValidator.class,
             converter = EncryptModeConverter.class,
             description = "The mode of data encryption in the pack file. It uses the best AES-GCM 256bit, which is also used in government. I recommend encrypt all, performance degradation is not so severe. When encrypting all, at least some data compression is recommended.")
  private EncryptMode encryptMode = PackConfig.DEFAULT_ENCRYPT_MODE;

  @Parameter(names = ROT128,
             order = 25,
             arity = 1,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "Apply rot128 to data. This is an additional layer of security to prevent the user from reading the file. It has low performance impacts. It can still be turned on.")
  private boolean rot128 = PackConfig.DEFAULT_ROT128;

  @Parameter(names = SHUFFLE,
             order = 26,
             arity = 1,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "Shuffling data is another layer to prevent data from being read and used. The principle is to randomly reorder bytes in buffers. Each file has its own key, by which it is possible to re-order the data to its original state. It has a big impact on performance (about 3 times slower according to the buffer size), even on unpacking a pack file.")
  private boolean shuffle = PackConfig.DEFAULT_SHUFFLE;

  @Parameter(names = PASSWORD_REQ,
             order = 27,
             arity = 1,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "The pack file will require a secret key to unpack. Without knowing the secret key, no one, nor the government will be able to open the pack.")
  private boolean passwordRequired = !PackConfig.DEFAULT_PASSWORDLESS;

  @Parameter(names = BUFFER_SIZE,
             order = 28,
             validateWith = CommonValidator.class,
             converter = IntegerConverter.class,
             description = "Buffer size to create pack file. The size may vary depending on the type of application settings and file types in the directory. A higher value means better compression but longer processing. Too low value, very low compression and also long processing. The ideal range is between 8KB and 2MB depending on the application settings.")
  private int bufferSize = PackConfig.DEFAULT_BUFFER_SIZE;

  @Parameter(names = DELETE_SOURCE,
             order = 29,
             arity = 1,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "Deletes the source. If you create a pack file, it will delete the entire directory. If you unpack a pack file, it will delete the pack file itself.")
  private boolean deleteSource = PackConfig.DEFAULT_DELETE_DIRECTORY_AFTER_FINISH;

  @Parameter(names = DETAIL,
             order = 30,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "For INFO mode, it also displays the contents of the pack file. Lists the directory tree of a saved pack file.")
  private boolean detail = false;

  @Parameter(names = SILENT,
             order = 31,
             validateWith = CommonValidator.class,
             converter = BoolConverter.class,
             description = "This mode does not write anything to the console. It simply creates or unpacks the pack file in the background. I do not recommend using it with the -pass parameter.")
  private boolean silent = false;

  @Parameter(names = HELP, help = true, order = 999, description = "Displays this help.")
  private boolean help;

  public List<String> getParameters() {
    return parameters;
  }

  public String getInput() {
    return input;
  }

  public void setInput(String input) {
    this.input = input;
  }

  public String getOutput() {
    return output;
  }

  public void setOutput(String output) {
    this.output = output;
  }

  public String getSecretkey() {
    return secretkey;
  }

  public void setSecretkey(String secretkey) {
    this.secretkey = secretkey;
  }

  public boolean isHelp() {
    return help;
  }

  public void setHelp(boolean help) {
    this.help = help;
  }

  public void setParameters(List<String> parameters) {
    this.parameters = parameters;
  }

  public boolean isMultithreaded() {
    return multithreaded;
  }

  public void setMultithreaded(boolean multithreaded) {
    this.multithreaded = multithreaded;
  }

  public CompressMode getCompressMode() {
    return compressMode;
  }

  public void setCompressMode(CompressMode compressMode) {
    this.compressMode = compressMode;
  }

  public int getCompressLevel() {
    return compressLevel;
  }

  public void setCompressLevel(int compressLevel) {
    this.compressLevel = compressLevel;
  }

  public EncryptMode getEncryptMode() {
    return encryptMode;
  }

  public void setEncryptMode(EncryptMode encryptMode) {
    this.encryptMode = encryptMode;
  }

  public boolean isRot128() {
    return rot128;
  }

  public void setRot128(boolean rot128) {
    this.rot128 = rot128;
  }

  public boolean isShuffle() {
    return shuffle;
  }

  public void setShuffle(boolean shuffle) {
    this.shuffle = shuffle;
  }

  public boolean isPasswordRequired() {
    return passwordRequired;
  }

  public void setPasswordRequired(boolean passwordRequired) {
    this.passwordRequired = passwordRequired;
  }

  public int getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(int bufferSize) {
    this.bufferSize = bufferSize;
  }

  public boolean isDeleteSource() {
    return deleteSource;
  }

  public void setDeleteSource(boolean deleteSource) {
    this.deleteSource = deleteSource;
  }

  public Mode getMode() {
    return mode;
  }

  public void setMode(Mode mode) {
    this.mode = mode;
  }

  public boolean isDetail() {
    return detail;
  }

  public void setDetail(boolean detail) {
    this.detail = detail;
  }

  public boolean isSilent() {
    return silent;
  }

  public void setSilent(boolean silent) {
    this.silent = silent;
  }
}
