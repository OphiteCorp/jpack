package oc.mimic.jpack.params.data;

/**
 * Mód aplikace.
 *
 * @author mimic
 */
public enum Mode {

  /**
   * Vytvoření pack souboru.
   */
  PACK(new String[]{ "pack", "p" }),
  /**
   * Rozbalení pack souboru.
   */
  UNPACK(new String[]{ "unpack", "u" }),
  /**
   * Vypsání informací o pack souboru.
   */
  INFO(new String[]{ "info", "i" });

  private final String[] aliases;

  Mode(String[] aliases) {
    this.aliases = aliases;
  }

  /**
   * Získá typ podle aliasu.
   *
   * @param alias Alias módu.
   *
   * @return Typ nebo null.
   */
  public static Mode getByAlias(String alias) {
    for (var type : values()) {
      for (var a : type.aliases) {
        if (a.equalsIgnoreCase(alias)) {
          return type;
        }
      }
    }
    return null;
  }

  public String[] getAliases() {
    return aliases;
  }
}
