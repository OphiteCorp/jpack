package oc.mimic.jpack.struct;

import java.io.File;

/**
 * Informace o rozbaleném pack souboru.
 *
 * @author mimic
 */
public final class Unpack {

  private UnpackConfig config; // použitá konfigurace
  private PackHeaderFull headerFull; // hlavička pack souboru
  private File outputDirectory; // adresář, kam byl pack rozbalen
  private File packFile; // nové vytvořený pack soubor
  private long elapsedTime; // uplynulý čas v ms

  public UnpackConfig getConfig() {
    return config;
  }

  public void setConfig(UnpackConfig config) {
    this.config = config;
  }

  public PackHeaderFull getHeaderFull() {
    return headerFull;
  }

  public void setHeaderFull(PackHeaderFull headerFull) {
    this.headerFull = headerFull;
  }

  public File getOutputDirectory() {
    return outputDirectory;
  }

  public void setOutputDirectory(File outputDirectory) {
    this.outputDirectory = outputDirectory;
  }

  public File getPackFile() {
    return packFile;
  }

  public void setPackFile(File packFile) {
    this.packFile = packFile;
  }

  public long getElapsedTime() {
    return elapsedTime;
  }

  public void setElapsedTime(long elapsedTime) {
    this.elapsedTime = elapsedTime;
  }

  @Override
  public String toString() {
    return outputDirectory.toString();
  }
}

