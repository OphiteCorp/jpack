package oc.mimic.jpack.struct;

import oc.mimic.jpack.component.PercentFormatter;

import java.io.File;

/**
 * Informace s průběhem čtení souboru.
 *
 * @author mimic
 */
public final class FileProgressReadInfo {

  private PackHeaderFile headerFile; // informace o souboru z pack souboru
  private File outputFile; // výstupní soubor, kam se má soubor z packu uložit
  private long threadId; // číslo vlákna
  private long pointerPos; // aktuální pozice pointeru v souboru
  private int currentRead; // aktuálně přečteno dat
  private boolean finished; // je soubor kompletně načten?

  /**
   * Získá celkový procenta.
   *
   * @return Kolik procent je hotovo.
   */
  public double getTotalPercent() {
    return (100. / headerFile.getFilesize().longValue()) * pointerPos;
  }

  /**
   * Získá celkový procenta.
   *
   * @return Kolik procent je hotovo.
   */
  public String getTotalPercentStr() {
    return PercentFormatter.format(getTotalPercent());
  }

  public boolean isFinished() {
    return finished;
  }

  public void setFinished(boolean finished) {
    this.finished = finished;
  }

  public PackHeaderFile getHeaderFile() {
    return headerFile;
  }

  public void setHeaderFile(PackHeaderFile headerFile) {
    this.headerFile = headerFile;
  }

  public File getOutputFile() {
    return outputFile;
  }

  public void setOutputFile(File outputFile) {
    this.outputFile = outputFile;
  }

  public long getPointerPos() {
    return pointerPos;
  }

  public void setPointerPos(long pointerPos) {
    this.pointerPos = pointerPos;
  }

  public long getThreadId() {
    return threadId;
  }

  public void setThreadId(long threadId) {
    this.threadId = threadId;
  }

  public int getCurrentRead() {
    return currentRead;
  }

  public void setCurrentRead(int currentRead) {
    this.currentRead = currentRead;
  }

  @Override
  public String toString() {
    return "[" + threadId + "][" + pointerPos + "/" + getHeaderFile().getFilesize() + "][" + getTotalPercentStr() +
           "] " + headerFile.getRelativePath();
  }
}
