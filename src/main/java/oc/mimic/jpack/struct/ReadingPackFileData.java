package oc.mimic.jpack.struct;

/**
 * Informace o čteném pack souboru.
 *
 * @author mimic
 */
public final class ReadingPackFileData {

  private PackHeaderFull headerFull; // hlavička pack souboru
  private long totalRead; // celkový počet přečtených dat
  private int filesDone; // celkový počet přečtených souborů
  private int activeThreads; // počet aktivních čtecích vláken

  public PackHeaderFull getHeaderFull() {
    return headerFull;
  }

  public void setHeaderFull(PackHeaderFull headerFull) {
    this.headerFull = headerFull;
  }

  public long getTotalRead() {
    return totalRead;
  }

  public void setTotalRead(long totalRead) {
    this.totalRead = totalRead;
  }

  public int getFilesDone() {
    return filesDone;
  }

  public void setFilesDone(int filesDone) {
    this.filesDone = filesDone;
  }

  public int getActiveThreads() {
    return activeThreads;
  }

  public void setActiveThreads(int activeThreads) {
    this.activeThreads = activeThreads;
  }
}
