package oc.mimic.jpack.struct;

import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Hlavička s informacemi o packu. Ukládá se vždy na začátek souboru v packu.
 *
 * @author mimic
 */
public final class PackHeader {

  private final List<PackHeaderFile> files; // soubory a adresáře z kořenového adresáře
  private Number totalFiles; // celkový počet souborů
  private Number totalFilesize; // celková velikost všech souborů
  private Number totalDirectories; // celkový počet všech adresářů
  private Number bufferSize; // velikost bufferu
  private byte compressMode; // mód komprese
  private byte encryptMode; // mód encryptu
  private boolean rot128; // je zapnut rot128?
  private boolean shuffle; // je zapnut shuffle?
  private long createdDateTime; // datum vytvoření
  private Number packingTime; // čas zabalení pack souboru
  private Number usedThreads; // použitých vláken

  /**
   * Vytvoří novou instanci hlavičky.
   */
  public PackHeader() {
    files = new ArrayList<>();
  }

  public EncryptMode getEncryptModeType() {
    return EncryptMode.getByCode(encryptMode);
  }

  public CompressMode getCompressModeType() {
    return CompressMode.getByCode(compressMode);
  }

  public Number getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(Number totalFiles) {
    this.totalFiles = totalFiles;
  }

  public List<PackHeaderFile> getFiles() {
    return files;
  }

  public Number getTotalFilesize() {
    return totalFilesize;
  }

  public void setTotalFilesize(Number totalFilesize) {
    this.totalFilesize = totalFilesize;
  }

  public Number getTotalDirectories() {
    return totalDirectories;
  }

  public void setTotalDirectories(Number totalDirectories) {
    this.totalDirectories = totalDirectories;
  }

  public Number getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(Number bufferSize) {
    this.bufferSize = bufferSize;
  }

  public byte getCompressMode() {
    return compressMode;
  }

  public void setCompressMode(byte compressMode) {
    this.compressMode = compressMode;
  }

  public boolean isRot128() {
    return rot128;
  }

  public void setRot128(boolean rot128) {
    this.rot128 = rot128;
  }

  public byte getEncryptMode() {
    return encryptMode;
  }

  public void setEncryptMode(byte encryptMode) {
    this.encryptMode = encryptMode;
  }

  public boolean isShuffle() {
    return shuffle;
  }

  public void setShuffle(boolean shuffle) {
    this.shuffle = shuffle;
  }

  public long getCreatedDateTime() {
    return createdDateTime;
  }

  public void setCreatedDateTime(long createdDateTime) {
    this.createdDateTime = createdDateTime;
  }

  public Number getPackingTime() {
    return packingTime;
  }

  public void setPackingTime(Number packingTime) {
    this.packingTime = packingTime;
  }

  public Number getUsedThreads() {
    return usedThreads;
  }

  public void setUsedThreads(Number usedThreads) {
    this.usedThreads = usedThreads;
  }
}
