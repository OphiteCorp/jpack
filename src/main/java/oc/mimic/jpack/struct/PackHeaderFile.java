package oc.mimic.jpack.struct;

import java.util.Objects;

/**
 * Detailní informace o jednom souboru v hlavičce packu.
 *
 * @author mimic
 */
public final class PackHeaderFile {

  private transient final long index; // index souboru použitý pro indexaci a vyhledávání

  private Number filesize; // velikost souboru v bytech (adreáře mají 0)
  private String relativePath; // relativní cesta k souboru dle kořenového adresáře
  private byte[] shuffleSecret; // tajný kód pro unshuffle
  private long entryPoint; // pozice v pack souboru, kde začíná tento soubor
  private boolean directory; // jedná se o adresář?

  /**
   * Vytvoří novou instanci s informacemi o souboru pro hlavičku packu.
   *
   * @param index Index souboru.
   */
  public PackHeaderFile(long index) {
    this.index = index;
  }

  public Number getFilesize() {
    return filesize;
  }

  public void setFilesize(Number filesize) {
    this.filesize = filesize;
  }

  public String getRelativePath() {
    return relativePath;
  }

  public void setRelativePath(String relativePath) {
    this.relativePath = relativePath;
  }

  public long getEntryPoint() {
    return entryPoint;
  }

  public void setEntryPoint(long entryPoint) {
    this.entryPoint = entryPoint;
  }

  public long getIndex() {
    return index;
  }

  public boolean isDirectory() {
    return directory;
  }

  public void setDirectory(boolean directory) {
    this.directory = directory;
  }

  public byte[] getShuffleSecret() {
    return shuffleSecret;
  }

  public void setShuffleSecret(byte[] shuffleSecret) {
    this.shuffleSecret = shuffleSecret;
  }

  @Override
  public String toString() {
    return relativePath;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    var that = (PackHeaderFile) o;
    return index == that.index;
  }

  @Override
  public int hashCode() {
    return Objects.hash(index);
  }
}
