package oc.mimic.jpack.struct;

import oc.mimic.jpack.struct.enums.CompressMode;
import oc.mimic.jpack.struct.enums.EncryptMode;
import org.tukaani.xz.LZMA2Options;

import java.util.zip.Deflater;

/**
 * Konfigurace pro pack souboru.
 *
 * @author mimic
 */

public final class PackConfig {

  /**
   * Výchozí tajný klíč. Tento klíč není nastaven - vždy bude vygenerován náhodný. Klíč by měl odpovídat délce
   * 32 znaků v HEX formátu (0-9, A-F).
   */
  public static final String DEFAULT_SECRET_KEY = null;
  /**
   * Mód komprese pack souboru. Výchozí hodnota je {@link CompressMode#LZ4}, protože je velice rychlá a
   * má aspoň nějakou kompresi (lepší než žádná).
   */
  public static final CompressMode DEFAULT_COMPRESS_MODE = CompressMode.GZIP;
  /**
   * Mód encryptu. Určuje, co všechno se má encryptovat. Má malé dopady na výkon, takže je nastaveno vše.
   */
  public static final EncryptMode DEFAULT_ENCRYPT_MODE = EncryptMode.ALL;
  /**
   * Výchozí nastavení komprese. Každý typ komprese má jinou stupnici, takže je potřeba se řídit tím. Menší hodnota
   * je menší komprese. Např. GZIP má {@link Deflater#BEST_SPEED}-{@link Deflater#BEST_COMPRESSION}.
   * XZ zase {@link LZMA2Options#PRESET_MIN}-{@link LZMA2Options#PRESET_MAX}. Výchozí nastavení je nejnižší.
   */
  public static final int DEFAULT_COMPRESS_LEVEL = Deflater.BEST_SPEED;
  /**
   * Výchozí nastavení ROT128. Jedná se o přetočení byte v souborech. Má minimální (téměř řádný) dopady na výkon,
   * takže je ve výchozím řežimu povolený.
   */
  public static final boolean DEFAULT_ROT128 = true;
  /**
   * Shuffle byte v souborech. Tato logika náhodně přeuspořádá všechny byty při zápisu souboru do pack souboru.
   * Aby byty bylo možné narovnat zpět, tak je potřeba znát seed (uloží se do hlavičky), pod kterým byly data rozházený.
   * Ve výchozím režimu je to vypnuté, je velice (asi 3x) náročný na výkon. Je dobré u menších pack souborů.
   * Pokud se použije shuffle, je doporučeno použít aspoň nějakou kompresi.
   */
  public static final boolean DEFAULT_SHUFFLE = false;
  /**
   * Bude vyžadováno heslo k otevření pack souboru? Pokud bude True, tak nebude a heslo se uloží do pack souboru, aby
   * ho bylo možné otevřít bez znalosti hesla. V opačném případě je vyžadováno uživatelem.
   */
  public static final boolean DEFAULT_PASSWORDLESS = true;
  /**
   * Výchozí velikost bufferu. Ve výchozím stavu je nastavena optimální velikost. Velikost bufferu je důležitá ve více
   * směrech. Větší velikost zlepšuje vlastnosti komprese, ale zvyšuje nároky na HW. Při zapnutém shuffle je dobré to
   * nepřehánět s velikostí. S použitím shuffle je ideální mezi 8-128 KB, bez něho je možné jít až na 2MB. Větší
   * velikosti už nedávají smysl a nic se nezlepší. Ideálních je mezi 8KB až 2MB.
   */
  public static final int DEFAULT_BUFFER_SIZE = 1024 * 128; // 128 KB
  /**
   * Pro vytvoření pack souboru bude použito více jader CPU. Počet vláken je rovný počtu logických procesorů. Takže se
   * bere součet fyzických i virtuálních (pro 4 jádro s HT to bude 8 vláken, bez HT jen 4). Výchozí hodnota je True.
   */
  public static final boolean DEFAULT_MULTITHREADED = true;
  /**
   * Pokud bude True, tak se pokusí kompletně smazat celý adresář, ze kterého se vytvořil pack soubor.
   */
  public static final boolean DEFAULT_DELETE_DIRECTORY_AFTER_FINISH = false;

  private String secretKey = DEFAULT_SECRET_KEY;
  private CompressMode compressMode = DEFAULT_COMPRESS_MODE;
  private EncryptMode encryptMode = DEFAULT_ENCRYPT_MODE;
  private int compressLevel = DEFAULT_COMPRESS_LEVEL;
  private boolean rot128 = DEFAULT_ROT128;
  private boolean shuffle = DEFAULT_SHUFFLE;
  private boolean passwordless = DEFAULT_PASSWORDLESS;
  private int bufferSize = DEFAULT_BUFFER_SIZE;
  private boolean multithreaded = DEFAULT_MULTITHREADED;
  private boolean deleteDirectoryAfterFinish = DEFAULT_DELETE_DIRECTORY_AFTER_FINISH;

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public CompressMode getCompressMode() {
    return compressMode;
  }

  public void setCompressMode(CompressMode compressMode) {
    this.compressMode = compressMode;
  }

  public boolean isRot128() {
    return rot128;
  }

  public void setRot128(boolean rot128) {
    this.rot128 = rot128;
  }

  public boolean isPasswordless() {
    return passwordless;
  }

  public void setPasswordless(boolean passwordless) {
    this.passwordless = passwordless;
  }

  public EncryptMode getEncryptMode() {
    return encryptMode;
  }

  public void setEncryptMode(EncryptMode encryptMode) {
    this.encryptMode = encryptMode;
  }

  public int getBufferSize() {
    return bufferSize;
  }

  public void setBufferSize(int bufferSize) {
    this.bufferSize = bufferSize;
  }

  public boolean isMultithreaded() {
    return multithreaded;
  }

  public void setMultithreaded(boolean multithreaded) {
    this.multithreaded = multithreaded;
  }

  public boolean isShuffle() {
    return shuffle;
  }

  public void setShuffle(boolean shuffle) {
    this.shuffle = shuffle;
  }

  public int getCompressLevel() {
    return compressLevel;
  }

  public void setCompressLevel(int compressLevel) {
    this.compressLevel = compressLevel;
  }

  public boolean isDeleteDirectoryAfterFinish() {
    return deleteDirectoryAfterFinish;
  }

  public void setDeleteDirectoryAfterFinish(boolean deleteDirectoryAfterFinish) {
    this.deleteDirectoryAfterFinish = deleteDirectoryAfterFinish;
  }
}
