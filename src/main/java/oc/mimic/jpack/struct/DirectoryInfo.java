package oc.mimic.jpack.struct;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Informace o adresáři, ze kterého se má vytvořit pack.
 *
 * @author mimic
 */
public final class DirectoryInfo {

  private final File directory; // hlavní adresář
  private final List<FileInfo> files; // soubory a adresáře z hlavního adresáře
  private long totalFilesize; // celková velikost všech souborů (bez adresářů)
  private long totalDirectories; // celkový počet adresářů

  /**
   * Vytvoří novou instanci s informacemi o adresáři.
   *
   * @param directory Adresář.
   */
  public DirectoryInfo(File directory) {
    this.directory = directory;
    files = new ArrayList<>(0);
  }

  public void addFile(FileInfo fileInfo) {
    files.add(fileInfo);
  }

  public File getDirectory() {
    return directory;
  }

  public List<FileInfo> getFiles() {
    return files;
  }

  public long getTotalFilesize() {
    return totalFilesize;
  }

  public void setTotalFilesize(long totalFilesize) {
    this.totalFilesize = totalFilesize;
  }

  public long getTotalDirectories() {
    return totalDirectories;
  }

  public void setTotalDirectories(long totalDirectories) {
    this.totalDirectories = totalDirectories;
  }

  @Override
  public String toString() {
    return directory.toString();
  }
}
