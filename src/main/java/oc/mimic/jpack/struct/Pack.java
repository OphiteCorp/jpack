package oc.mimic.jpack.struct;

import java.io.File;

/**
 * Informace o vytvořeném pack souboru.
 *
 * @author mimic
 */
public final class Pack {

  private PackConfig config; // použitá konfigurace
  private PackHeaderFull headerFull; // hlavička pack souboru
  private DirectoryInfo dirInfo; // informace o adresáři
  private File packFile; // nové vytvořený pack soubor
  private String secretKey; // tajný klíč pro decrypt
  private long elapsedTime; // uplynulý čas v ms

  public PackConfig getConfig() {
    return config;
  }

  public void setConfig(PackConfig config) {
    this.config = config;
  }

  public File getPackFile() {
    return packFile;
  }

  public void setPackFile(File packFile) {
    this.packFile = packFile;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public long getElapsedTime() {
    return elapsedTime;
  }

  public void setElapsedTime(long elapsedTime) {
    this.elapsedTime = elapsedTime;
  }

  public PackHeaderFull getHeaderFull() {
    return headerFull;
  }

  public void setHeaderFull(PackHeaderFull headerFull) {
    this.headerFull = headerFull;
  }

  public DirectoryInfo getDirInfo() {
    return dirInfo;
  }

  public void setDirInfo(DirectoryInfo dirInfo) {
    this.dirInfo = dirInfo;
  }

  @Override
  public String toString() {
    return packFile.toString();
  }
}

