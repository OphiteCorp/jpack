package oc.mimic.jpack.struct.enums;

/**
 * Stav procesu pri pack/unpack.
 *
 * @author mimic
 */
public enum ProcessState {

  // Sdílené pro Pack, Unpack i Info
  //=======================================================================================

  /**
   * Process byl dokončen a aplikace je připravena pro další pack/unpack.
   */
  READY,

  /**
   * Proces pack/unpack byl zahájen.
   */
  STARTED,

  // Pouze pro: Pack
  //=======================================================================================

  /**
   * Probíhá skenování adresáře.
   */
  SCANNING,

  /**
   * Skenování adresáře bylo dokončeno.
   */
  SCANNING_COMPLETED,

  /**
   * Započla příprava hlavičky pack souboru.
   */
  PREPARING_PACK_HEADER,

  /**
   * Hlavička pack souboru byla připravena. V tuto chvíli stále není kompletní.
   */
  PREPARING_PACK_HEADER_COMPLETED,

  /**
   * Probíhá sestavování pack souboru.
   */
  BUILDING_PACK,

  /**
   * Byla vytvořena finální podoba hlavičky souboru.
   */
  CREATED_PACK_HEADER,

  // Pouze pro: Unpack
  //=======================================================================================

  /**
   * Otevírání hlavičky pack souboru.
   */
  OPENING_HEADER,

  /**
   * Hlavička pack souboru byla úspěšně otevřena.
   */
  OPENING_HEADER_COMPLETED,

  /**
   * Probíhá rozbalování packu.
   */
  UNPACKING
}
