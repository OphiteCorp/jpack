package oc.mimic.jpack.struct.enums;

import org.tukaani.xz.LZMA2Options;

import java.util.zip.Deflater;

/**
 * Úroveň komprese.
 *
 * @author mimic
 */
public enum CompressMode {

  /**
   * Bez komprese.
   */
  NONE((byte) 0, false, 0, 0, "none"),
  /**
   * Komprese pomocí LZ4 ve fast módu. Není důkladná jako GZIP. Hodně rychlá.
   */
  LZ4((byte) 1, false, 0, 0, "lz4"),
  /**
   * Komprese pomocí LZ4 v HC módu. Není důkladná jako GZIP. Lepší komprese než LZ4 fast. Je pomalejší,
   * ale rychlejší nez GZIP.
   */
  LZ4HC((byte) 2, false, 1, 17, "lz4hc"),
  /**
   * Komprese GZIP. Relativně dobrá kvalita. Je pomalá. Není tak dobrá, jako lepší algoritmy (LZMA, XZ, atd.)
   */
  GZIP((byte) 3, true, Deflater.BEST_SPEED, Deflater.BEST_COMPRESSION, "gzip"),
  /**
   * Komprese BZIP2. Lehce lepší kvalita než GZIP. Je trochu pomalejší. Jinak to samé jako GZIP.
   */
  BZIP2((byte) 4, false, 0, 0, "bzip2"),
  /**
   * Komprese XZ (obdoba LZMA2). Nejlepší kvalita. Je hodně pomalý i na nejnižší kompresi.
   * Na nejvyšší kompresi je potřeba více než 30GB RAM! Má smysl pouze na něco.
   */
  XZ((byte) 5, true, LZMA2Options.PRESET_MIN, LZMA2Options.PRESET_MAX, "xz");

  private final byte code;
  private final boolean hasLevel;
  private final int minLevel;
  private final int maxLevel;
  private final String alias;

  CompressMode(byte code, boolean hasLevel, int minLevel, int maxLevel, String alias) {
    this.code = code;
    this.hasLevel = hasLevel;
    this.minLevel = minLevel;
    this.maxLevel = maxLevel;
    this.alias = alias;
  }

  /**
   * Vyhledá mód podle kódu.
   *
   * @param code Kód módu.
   *
   * @return Mód nebo null.
   */
  public static CompressMode getByCode(byte code) {
    for (var mode : values()) {
      if (mode.code == code) {
        return mode;
      }
    }
    return null;
  }

  /**
   * Vyhledá mód podle aliasu.
   *
   * @param alias Alias módu.
   *
   * @return Mód nebo null.
   */
  public static CompressMode getByAlias(String alias) {
    for (var mode : values()) {
      if (mode.alias.equalsIgnoreCase(alias)) {
        return mode;
      }
    }
    return null;
  }

  /**
   * Získá minimální úroveň komprese mezi všemi typy.
   *
   * @return Úroveň komprese.
   */
  public static int minCompressLevel() {
    var min = Integer.MAX_VALUE;

    for (var type : values()) {
      if (type.minLevel < min) {
        min = type.minLevel;
      }
    }
    return min;
  }

  /**
   * Získá maximální úroveň komprese mezi všemi typy.
   *
   * @return Úroveň komprese.
   */
  public static int maxCompressLevel() {
    var max = Integer.MIN_VALUE;

    for (var type : values()) {
      if (type.maxLevel > max) {
        max = type.maxLevel;
      }
    }
    return max;
  }

  public byte getCode() {
    return code;
  }

  public boolean isHasLevel() {
    return hasLevel;
  }

  public int getMinLevel() {
    return minLevel;
  }

  public int getMaxLevel() {
    return maxLevel;
  }

  public String getAlias() {
    return alias;
  }
}
