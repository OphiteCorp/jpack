package oc.mimic.jpack.struct.enums;

/**
 * Typ encryptu pack souboru.
 *
 * @author mimic
 */
public enum EncryptMode {

  /**
   * Encrypt bude použit pouze na hlavičku souboru.
   */
  HEADER((byte) 0, "header"),

  /**
   * Encrypt se použije na vše (hlavička + soubory).
   */
  ALL((byte) 1, "all");

  private final byte code;
  private final String alias;

  EncryptMode(byte code, String alias) {
    this.code = code;
    this.alias = alias;
  }

  /**
   * Vyhledá mód podle kódu.
   *
   * @param code Kód módu.
   *
   * @return Mód nebo null.
   */
  public static EncryptMode getByCode(byte code) {
    for (var mode : values()) {
      if (mode.code == code) {
        return mode;
      }
    }
    return null;
  }

  /**
   * Vyhledá mód podle aliasu.
   *
   * @param alias Alias módu.
   *
   * @return Mód nebo null.
   */
  public static EncryptMode getByAlias(String alias) {
    for (var mode : values()) {
      if (mode.alias.equalsIgnoreCase(alias)) {
        return mode;
      }
    }
    return null;
  }

  public byte getCode() {
    return code;
  }

  public String getAlias() {
    return alias;
  }
}
