package oc.mimic.jpack.struct;

/**
 * Konfigurace pro unpack souboru.
 *
 * @author mimic
 */
public final class UnpackConfig {

  /**
   * Tajný klíč pro decrypt. Ignoruje se, pokud je pack bez hesla - passwordless. Výchozí hodnota je null.
   */
  public static final String DEFAULT_SECRET_KEY = PackConfig.DEFAULT_SECRET_KEY;
  /**
   * Výchozí použití vícevláknového rozbalení packu. Počet vláken se bere podle možností procesoru.
   * Výchozí hodnota je True.
   */
  public static final boolean DEFAULT_MULTITHREADED = PackConfig.DEFAULT_MULTITHREADED;
  /**
   * Pokud bude True, tak po rozbalení pack souboru ho smaže.
   */
  public static final boolean DEFAULT_DELETE_PACK_AFTER_FINISH = PackConfig.DEFAULT_DELETE_DIRECTORY_AFTER_FINISH;

  private String secretKey = DEFAULT_SECRET_KEY;
  private boolean multithreaded = DEFAULT_MULTITHREADED;
  private boolean deletePackAfterFinish = DEFAULT_DELETE_PACK_AFTER_FINISH;

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public boolean isMultithreaded() {
    return multithreaded;
  }

  public void setMultithreaded(boolean multithreaded) {
    this.multithreaded = multithreaded;
  }

  public boolean isDeletePackAfterFinish() {
    return deletePackAfterFinish;
  }

  public void setDeletePackAfterFinish(boolean deletePackAfterFinish) {
    this.deletePackAfterFinish = deletePackAfterFinish;
  }
}
