package oc.mimic.jpack.struct;

/**
 * Informace o hlavičce z načteného pack souboru.
 *
 * @author mimic
 */
public final class PackHeaderFull {

  private PackHeader header;
  private boolean passwordless; // je pack soubor bez hesla?
  private byte[] secretKey; // tajný klíč pro decrypt
  private short version; // verze aplikace ve které vznikl pack soubor.

  public PackHeader getHeader() {
    return header;
  }

  public void setHeader(PackHeader header) {
    this.header = header;
  }

  public boolean isPasswordless() {
    return passwordless;
  }

  public void setPasswordless(boolean passwordless) {
    this.passwordless = passwordless;
  }

  public byte[] getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(byte[] secretKey) {
    this.secretKey = secretKey;
  }

  public short getVersion() {
    return version;
  }

  public void setVersion(short version) {
    this.version = version;
  }
}
