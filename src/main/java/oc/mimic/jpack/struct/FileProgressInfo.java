package oc.mimic.jpack.struct;

import oc.mimic.jpack.component.PercentFormatter;

/**
 * Informace o průběhu souboru při zápisu do pack souboru.
 *
 * @author mimic
 */
public final class FileProgressInfo {

  private int fileNumber; // číslo souboru v seznamu (index)
  private int totalFiles; // celkový počet souborů
  private long pointerPos; // aktuální pozice pointeru v souboru

  /**
   * Získá celkový procenta.
   *
   * @return Kolik procent je hotovo.
   */
  public double getTotalPercent() {
    return (100. / totalFiles) * fileNumber;
  }

  /**
   * Získá celkový procenta.
   *
   * @return Kolik procent je hotovo.
   */
  public String getTotalPercentStr() {
    return PercentFormatter.format(getTotalPercent());
  }

  public int getFileNumber() {
    return fileNumber;
  }

  public void setFileNumber(int fileNumber) {
    this.fileNumber = fileNumber;
  }

  public int getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  public long getPointerPos() {
    return pointerPos;
  }

  public void setPointerPos(long pointerPos) {
    this.pointerPos = pointerPos;
  }

  @Override
  public String toString() {
    return "[" + fileNumber + "/" + totalFiles + "][" + getTotalPercentStr() + "]";
  }
}
