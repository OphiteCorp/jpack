package oc.mimic.jpack.struct;

import java.nio.file.Path;

/**
 * Informace o souboru nebo adresáři.
 *
 * @author mimic
 */
public final class FileInfo {

  private transient final long index; // index souboru použitý pro indexaci a vyhledávání

  private final Path filePath; // absolutní cesta k souboru na disku
  private String relativePath; // relativní cesta k souboru dle kořenového adresáře
  private long filesize; // velikost souboru v bytech (adreáře mají 0)
  private boolean directory; // jedná se o adresář?

  /**
   * Vytvoří novou instanci s informacemi o souboru.
   *
   * @param filePath Cesta k souboru.
   * @param index    Index souboru.
   */
  public FileInfo(Path filePath, long index) {
    this.filePath = filePath;
    this.index = index;
  }

  public Path getFilePath() {
    return filePath;
  }

  public String getRelativePath() {
    return relativePath;
  }

  public void setRelativePath(String relativePath) {
    this.relativePath = relativePath;
  }

  public long getFilesize() {
    return filesize;
  }

  public void setFilesize(long filesize) {
    this.filesize = filesize;
  }

  public long getIndex() {
    return index;
  }

  public boolean isDirectory() {
    return directory;
  }

  public void setDirectory(boolean directory) {
    this.directory = directory;
  }

  @Override
  public String toString() {
    return filePath.toString();
  }
}
