package oc.mimic.jpack.helper;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Pomocná třída pro práci s převody primitivních datových typů do pole bytů a zpět (pro IO operace).
 *
 * @author mimic
 */
public final class IOBytesHelper {

  private static final Charset STRING_CHARSET = StandardCharsets.UTF_8;

  // pomocné převodní tabulky
  private static final Map<Byte, NumberFlag> FLAGS;
  private static final Map<Class<?>, Byte> FLAGS_BY_CLASS;

  static {
    var fByte = new NumberFlag(Byte.class, 0, Byte.BYTES);
    var fShort = new NumberFlag(Short.class, 1, Short.BYTES);
    var fInteger = new NumberFlag(Integer.class, 2, Integer.BYTES);
    var fFloat = new NumberFlag(Float.class, 3, Float.BYTES);
    var fDouble = new NumberFlag(Double.class, 4, Double.BYTES);
    var fLong = new NumberFlag(Long.class, 5, Long.BYTES);

    FLAGS = new HashMap<>(6);
    FLAGS.put(fByte.code, fByte);
    FLAGS.put(fShort.code, fShort);
    FLAGS.put(fInteger.code, fInteger);
    FLAGS.put(fFloat.code, fFloat);
    FLAGS.put(fDouble.code, fDouble);
    FLAGS.put(fLong.code, fLong);

    FLAGS_BY_CLASS = new HashMap<>(6);
    FLAGS_BY_CLASS.put(fByte.clazz, fByte.code);
    FLAGS_BY_CLASS.put(fShort.clazz, fShort.code);
    FLAGS_BY_CLASS.put(fInteger.clazz, fInteger.code);
    FLAGS_BY_CLASS.put(fFloat.clazz, fFloat.code);
    FLAGS_BY_CLASS.put(fDouble.clazz, fDouble.code);
    FLAGS_BY_CLASS.put(fLong.clazz, fLong.code);
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param number Vstupní číslo.
   *
   * @return Pole bytů. Obsahuje [flag - 1 byte, číslo].
   */
  public static byte[] toNumberBytes(Number number) {
    if (number == null) {
      throw new IllegalArgumentException("Input number is null.");
    }
    // reálné číslo float bude mít vždy délku 1+4 byte
    if (number instanceof Float) {
      var bytes = toRawBytes(number.floatValue());
      var flag = FLAGS.get(getNumberFlagCode(number));
      var result = new byte[1 + flag.length];
      result[0] = flag.code;
      System.arraycopy(bytes, 0, result, 1, bytes.length);
      return result;
    }
    // reálné číslo double bude mít vždy délku 1+8 byte
    if (number instanceof Double) {
      var bytes = toRawBytes(number.doubleValue());
      var flag = FLAGS.get(getNumberFlagCode(number));
      var result = new byte[1 + flag.length];
      result[0] = flag.code;
      System.arraycopy(bytes, 0, result, 1, bytes.length);
      return result;
    }
    // ostatní celá čísla se vždy vyhodnotí podle jejich délky
    // příklad: pro dvoumístné číslo se nepožije typ int, ale byte, který má délku 1 byte místo 4 pro int
    var flag = FLAGS.get(getNumberFlagCode(number));
    if (flag.clazz.equals(Byte.class)) {
      return new byte[]{ flag.code, number.byteValue() };
    }
    if (flag.clazz.equals(Short.class)) {
      var bytes = toRawBytes(number.shortValue());
      var result = new byte[1 + flag.length];
      result[0] = flag.code;
      System.arraycopy(bytes, 0, result, 1, bytes.length);
      return result;
    }
    if (flag.clazz.equals(Integer.class)) {
      var bytes = toRawBytes(number.intValue());
      var result = new byte[1 + flag.length];
      result[0] = flag.code;
      System.arraycopy(bytes, 0, result, 1, bytes.length);
      return result;
    }
    var bytes = toRawBytes(number.longValue());
    var result = new byte[1 + flag.length];
    result[0] = flag.code;
    System.arraycopy(bytes, 0, result, 1, bytes.length);
    return result;
  }

  /**
   * Převede vstupní pole bytů na číslo.
   *
   * @param bytes Vstupní pole bytů [flag - 1 byte, číslo].
   *
   * @return Číslo.
   */
  public static Number fromNumberBytes(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    // minimální délka jsou 2 byte, protože první je flag a druhý (a další) číslo
    if (bytes.length < 2) {
      throw new IllegalArgumentException(
              "The input length does not match the number data type. The input has " + bytes.length + " bytes.");
    }
    var flag = getNumberFlag(bytes[0]);
    if (flag.clazz.equals(Byte.class)) {
      return bytes[1];
    }
    if (flag.clazz.equals(Short.class)) {
      return fromRawShort(Arrays.copyOfRange(bytes, 1, flag.length + 1));
    }
    if (flag.clazz.equals(Integer.class)) {
      return fromRawInteger(Arrays.copyOfRange(bytes, 1, flag.length + 1));
    }
    if (flag.clazz.equals(Float.class)) {
      return fromRawFloat(Arrays.copyOfRange(bytes, 1, flag.length + 1));
    }
    if (flag.clazz.equals(Double.class)) {
      return fromRawDouble(Arrays.copyOfRange(bytes, 1, flag.length + 1));
    }
    if (flag.clazz.equals(Long.class)) {
      return fromRawLong(Arrays.copyOfRange(bytes, 1, flag.length + 1));
    }
    throw new IllegalArgumentException("Unsupported input bytes: " + Arrays.toString(bytes));
  }

  /**
   * Převede vstupní text (UTF-8) do pole bytů.
   *
   * @param text Vstupní text.
   *
   * @return Pole bytů. Obsahuje [flag - 1 byte, délku textu, text].
   */
  public static byte[] toStringBytes(String text) {
    if (text == null) {
      throw new IllegalArgumentException("Input string is null.");
    }
    var bytes = text.getBytes(STRING_CHARSET);
    var textLength = toNumberBytes(bytes.length);
    var result = new byte[textLength.length + bytes.length];
    System.arraycopy(textLength, 0, result, 0, textLength.length);
    System.arraycopy(bytes, 0, result, textLength.length, bytes.length);
    return result;
  }

  /**
   * Převede vstupní pole bytů na text.
   *
   * @param bytes Vstupní pole bytů [flag - 1 byte, délku textu, text].
   *
   * @return Text (UTF-8).
   */
  public static String fromStringBytes(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    // minimální délka jsou 2 byte, protože první je flag pro délku textu a druhý je délka
    if (bytes.length < 2) {
      throw new IllegalArgumentException(
              "The input length does not match the string data type. The input has " + bytes.length + " bytes.");
    }
    var flag = getNumberFlag(bytes[0]);
    var textLength = fromNumberBytes(bytes);
    var textBytes = Arrays.copyOfRange(bytes, flag.length + 1, textLength.intValue() + flag.length + 1);

    return new String(textBytes, STRING_CHARSET);
  }

  /**
   * Převede pole bytů do char.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 2.
   *
   * @return Character hodnota.
   */
  public static char fromRawCharacter(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Character.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the char data type, which has " + Character.BYTES +
              " bytes. The input has " + bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Character.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getChar();
  }

  /**
   * Převede byte na hodnotu boolean.
   *
   * @param b Vstupní byte.
   *
   * @return Boolean hodnota.
   */
  public static boolean fromRawBoolean(byte b) {
    return (b == (byte) 1);
  }

  /**
   * Převede pole bytů na hodnotu boolean.
   *
   * @param bytes Vstupní pole bytů.
   *
   * @return Boolean hodnota.
   */
  public static boolean fromRawBoolean(byte[] bytes) {
    if (bytes == null || bytes.length == 0) {
      throw new IllegalArgumentException("Input bytes are null or empty.");
    }
    return (bytes[0] == (byte) 1);
  }

  /**
   * Převede pole bytů na hodnotu short.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 2.
   *
   * @return Short hodnota.
   */
  public static short fromRawShort(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Short.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the short data type, which has " + Short.BYTES +
              " bytes. The input has " + bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Short.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getShort();
  }

  /**
   * Převede pole bytů na hodnotu int.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 4.
   *
   * @return Int hodnota.
   */
  public static int fromRawInteger(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Integer.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the int data type, which has " + Integer.BYTES +
              " bytes. The input has " + bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Integer.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getInt();
  }

  /**
   * Převede pole bytů na hodnotu float.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 4.
   *
   * @return Float hodnota.
   */
  public static float fromRawFloat(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Float.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the float data type, which has " + Float.BYTES +
              " bytes. The input has " + bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Float.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getFloat();
  }

  /**
   * Převede pole bytů na hodnotu double.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 8.
   *
   * @return Double hodnota.
   */
  public static double fromRawDouble(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Double.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the double data type, which has " + Double.BYTES +
              " bytes. The input has " + bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Double.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getDouble();
  }

  /**
   * Převede pole bytů na hodnotu long.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 8.
   *
   * @return Long hodnota.
   */
  public static long fromRawLong(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Long.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the long data type, which has " + Long.BYTES + " bytes. The input has " +
              bytes.length + " bytes.");
    }
    var buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.put(bytes);
    buffer.flip();
    return buffer.getLong();
  }

  /**
   * Převede pole bytů na hodnotu string.
   *
   * @param bytes Vstupní pole bytů.
   *
   * @return String hodnota.
   */
  public static String fromRawString(byte[] bytes, Charset charset) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    return new String(bytes, charset);
  }

  /**
   * Převede pole bytů na hodnotu string.
   *
   * @param bytes Vstupní pole bytů.
   *
   * @return String hodnota (UTF-8).
   */
  public static String fromRawString(byte[] bytes) {
    return fromRawString(bytes, STRING_CHARSET);
  }

  /**
   * Převede pole bytů na hodnotu byte.
   *
   * @param bytes Vstupní pole bytů. Musí mít velikost 1.
   *
   * @return Byte hodnota.
   */
  public static byte fromRawByte(byte[] bytes) {
    if (bytes == null) {
      throw new IllegalArgumentException("Input bytes are null.");
    }
    if (bytes.length != Byte.BYTES) {
      throw new IllegalArgumentException(
              "The input length does not match the byte data type, which has " + Byte.BYTES + " bytes. The input has " +
              bytes.length + " bytes.");
    }
    return bytes[0];
  }

  /**
   * Převede znak do pole bytů.
   *
   * @param c Vstupní znak.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(char c) {
    var buffer = ByteBuffer.allocate(Character.BYTES);
    buffer.putChar(c);
    return buffer.array();
  }

  /**
   * Převede text do pole bytů.
   *
   * @param text    Vstupní text.
   * @param charset Znaková sada.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(String text, Charset charset) {
    if (text == null) {
      throw new IllegalArgumentException("Input text is null.");
    }
    return text.getBytes(charset);
  }

  /**
   * Převede text do pole bytů.
   *
   * @param text Vstupní text.
   *
   * @return Pole bytů (UTF-8).
   */
  public static byte[] toRawBytes(String text) {
    return toRawBytes(text, STRING_CHARSET);
  }

  /**
   * Převede jeden byte do pole bytů.
   *
   * @param b Byte hodnota.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(byte b) {
    return new byte[]{ b };
  }

  /**
   * Převede boolean do byte.
   *
   * @param value Boolean hodnota.
   *
   * @return Jeden byte.
   */
  public static byte toRawByte(boolean value) {
    return (byte) (value ? 1 : 0);
  }

  /**
   * Převede boolean do pole bytů.
   *
   * @param value Boolean hodnota.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(boolean value) {
    return new byte[]{ (byte) (value ? 1 : 0) };
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param value Vstupní číslo.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(short value) {
    var buffer = ByteBuffer.allocate(Short.BYTES);
    buffer.putShort(value);
    return buffer.array();
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param value Vstupní číslo.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(int value) {
    var buffer = ByteBuffer.allocate(Integer.BYTES);
    buffer.putInt(value);
    return buffer.array();
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param value Vstupní číslo.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(float value) {
    var buffer = ByteBuffer.allocate(Float.BYTES);
    buffer.putFloat(value);
    return buffer.array();
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param value Vstupní číslo.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(double value) {
    var buffer = ByteBuffer.allocate(Double.BYTES);
    buffer.putDouble(value);
    return buffer.array();
  }

  /**
   * Převede číslo do pole bytů.
   *
   * @param value Vstupní číslo.
   *
   * @return Pole bytů.
   */
  public static byte[] toRawBytes(long value) {
    var buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(value);
    return buffer.array();
  }

  /**
   * Získá délku (počet bytů), které flag reprezentuje.
   *
   * @param numberFlagCode Vstupní flag kód.
   *
   * @return Počet následujících bytů, ve kterých se drží hodnota.
   */
  public static byte getNumberFlagLength(byte numberFlagCode) {
    return getNumberFlag(numberFlagCode).length;
  }

  /**
   * Vyhodnotí a získá kód flagu pro číselný typ a hodnotu.
   *
   * @param number Vstupní číslo.
   *
   * @return Kód flagu vstupního čísla.
   */
  public static byte getNumberFlagCode(Number number) {
    if (number == null) {
      throw new IllegalArgumentException("Input number is null.");
    }
    // pro float a double (reálná čísla) se ponechá původní bytová délka, což je 4 a 8 byte
    if (number instanceof Float || number instanceof Double) {
      return FLAGS_BY_CLASS.get(number.getClass());
    }
    // pro ostatní typy se vypočte na základě hodnoty (pouze celá čísla)
    var value = number.longValue();
    if (value >= Byte.MIN_VALUE && value <= Byte.MAX_VALUE) {
      return FLAGS_BY_CLASS.get(Byte.class);
    }
    if (value >= Short.MIN_VALUE && value <= Short.MAX_VALUE) {
      return FLAGS_BY_CLASS.get(Short.class);
    }
    if (value >= Integer.MIN_VALUE && value <= Integer.MAX_VALUE) {
      return FLAGS_BY_CLASS.get(Integer.class);
    }
    return FLAGS_BY_CLASS.get(Long.class);
  }

  /**
   * Obrátí pořadí byte v poli.
   *
   * @param array Vstupní pole bytů.
   */
  public static void reverse(byte[] array) {
    for (var i = 0; i < array.length / 2; i++) {
      var temp = array[i];
      array[i] = array[array.length - 1 - i];
      array[array.length - 1 - i] = temp;
    }
  }

  /**
   * ROT128 pro pole bytů.
   *
   * @param bytes Vstupní data.
   *
   * @return Upravená data.
   */
  public static byte[] rot128(byte[] bytes) {
    var i = bytes.length;
    while (i-- > 0) {
      bytes[i] += 128;
    }
    return bytes;
  }

  /**
   * Získá číselný flag podle kódu flagu.
   *
   * @param b Vstupní byte s kódem flagu.
   *
   * @return Instance číselného flagu.
   */
  private static NumberFlag getNumberFlag(byte b) {
    var flag = FLAGS.get(b);
    if (flag == null) {
      throw new IllegalArgumentException("Input byte '" + b + "' not found among flags.");
    }
    return flag;
  }

  /**
   * Informace o flagu pro číslo.
   */
  private static final class NumberFlag {

    private final Class<?> clazz;
    private final byte code;
    private final byte length;

    private NumberFlag(Class<?> clazz, int code, int length) {
      this.clazz = clazz;
      this.code = (byte) code;
      this.length = (byte) length;
    }
  }
}
