package oc.mimic.jpack.helper;

import java.io.*;
import java.nio.charset.Charset;

/**
 * Vlastní stream pro práci s IOBytes.
 *
 * @author mimic
 */
public final class IOBytesStream {

  /**
   * Velikost flagu pro číslo. Hodnota v tomto flagu udává reálnou velikost čísla v bytech.
   */
  private static final int NUMBER_FLAG_LENGTH = 1;

  /**
   * Vstupní stream. Vychází z {@link BufferedInputStream}.
   */
  public static final class Input extends BufferedInputStream {

    /**
     * Vytvoří instanci streamu.
     *
     * @param in Vstupní stream.
     */
    public Input(InputStream in) {
      super(in);
    }

    /**
     * Vytvoří instanci streamu.
     *
     * @param in   Vstupní stream.
     * @param size Velikost streamu.
     */
    public Input(InputStream in, int size) {
      super(in, size);
    }

    /**
     * Načte číslo.
     *
     * @return Číslo.
     */
    public Number readNumber() throws IOException {
      var numberFlag = (byte) read();
      var numberFlagLength = IOBytesHelper.getNumberFlagLength(numberFlag);
      var numberLengthBytes = readNBytes(numberFlagLength);

      var bytes = new byte[numberFlagLength + NUMBER_FLAG_LENGTH];
      bytes[0] = numberFlag;
      System.arraycopy(numberLengthBytes, 0, bytes, NUMBER_FLAG_LENGTH, numberLengthBytes.length);

      return IOBytesHelper.fromNumberBytes(bytes);
    }

    /**
     * Načte text.
     *
     * @return Text.
     */
    public String readString() throws IOException {
      var textLength = readNumber().intValue();
      var textBytes = readNBytes(textLength);

      return IOBytesHelper.fromRawString(textBytes);
    }

    /**
     * Načte hodnotu boolean.
     *
     * @return Boolean hodnota.
     */
    public boolean readRawBoolean() throws IOException {
      var value = (byte) read();
      return IOBytesHelper.fromRawBoolean(value);
    }

    /**
     * Načte hodnotu char.
     *
     * @return Character hodnota.
     */
    public char readRawCharacter() throws IOException {
      var bytes = readNBytes(Character.BYTES);
      return IOBytesHelper.fromRawCharacter(bytes);
    }

    /**
     * Načte hodnotu short.
     *
     * @return Hodnota.
     */
    public short readRawShort() throws IOException {
      var bytes = readNBytes(Short.BYTES);
      return IOBytesHelper.fromRawShort(bytes);
    }

    /**
     * Načte hodnotu integer.
     *
     * @return Hodnota.
     */
    public int readRawInteger() throws IOException {
      var bytes = readNBytes(Integer.BYTES);
      return IOBytesHelper.fromRawInteger(bytes);
    }

    /**
     * Načte hodnotu float.
     *
     * @return Hodnota.
     */
    public float readRawFloat() throws IOException {
      var bytes = readNBytes(Float.BYTES);
      return IOBytesHelper.fromRawFloat(bytes);
    }

    /**
     * Načte hodnotu double.
     *
     * @return Hodnota.
     */
    public double readRawDouble() throws IOException {
      var bytes = readNBytes(Double.BYTES);
      return IOBytesHelper.fromRawDouble(bytes);
    }

    /**
     * Načte hodnotu long.
     *
     * @return Hodnota.
     */
    public long readRawLong() throws IOException {
      var bytes = readNBytes(Long.BYTES);
      return IOBytesHelper.fromRawLong(bytes);
    }

    /**
     * Načte byte hodnotu.
     *
     * @return Hodnota.
     */
    public byte readRawByte() throws IOException {
      var bytes = readNBytes(Byte.BYTES);
      return IOBytesHelper.fromRawByte(bytes);
    }

    /**
     * Načte hodnotu string.
     *
     * @param textLengthInBytes Délka textu v bytech.
     * @param charset           Znaková sada.
     *
     * @return Text.
     */
    public String readRawString(int textLengthInBytes, Charset charset) throws IOException {
      var bytes = readNBytes(textLengthInBytes);
      if (charset == null) {
        return IOBytesHelper.fromRawString(bytes);
      } else {
        return IOBytesHelper.fromRawString(bytes, charset);
      }
    }

    /**
     * Načte hodnotu string.
     *
     * @param textLengthInBytes Délka textu v bytech.
     *
     * @return Text.
     */
    public String readRawString(int textLengthInBytes) throws IOException {
      return readRawString(textLengthInBytes, null);
    }
  }

  /**
   * Výstupní stream. Vychází z {@link BufferedOutputStream}.
   */
  public static final class Output extends BufferedOutputStream {

    /**
     * Vytvoří instanci streamu.
     *
     * @param out Výstupní stream.
     */
    public Output(OutputStream out) {
      super(out);
    }

    /**
     * Vytvoří instanci streamu.
     *
     * @param out  Výstupní stream.
     * @param size Velikost streamu.
     */
    public Output(OutputStream out, int size) {
      super(out, size);
    }

    /**
     * Zapíše číslo.
     *
     * @param number Číslo.
     */
    public void writeNumber(Number number) throws IOException {
      var bytes = IOBytesHelper.toNumberBytes(number);
      write(bytes);
    }

    /**
     * Zapíše text.
     *
     * @param text Text.
     */
    public void writeString(String text) throws IOException {
      var bytes = IOBytesHelper.toStringBytes(text);
      write(bytes);
    }

    /**
     * Zapíše boolean hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawBoolean(boolean value) throws IOException {
      var b = IOBytesHelper.toRawByte(value);
      write(b);
    }

    /**
     * Zapíše znak.
     *
     * @param c Znak.
     */
    public void writeRawCharacter(char c) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(c);
      write(bytes);
    }

    /**
     * Zapíše číselnou hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawShort(short value) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(value);
      write(bytes);
    }

    /**
     * Zapíše číselnou hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawInteger(int value) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(value);
      write(bytes);
    }

    /**
     * Zapíše číselnou hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawFloat(float value) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(value);
      write(bytes);
    }

    /**
     * Zapíše číselnou hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawDouble(double value) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(value);
      write(bytes);
    }

    /**
     * Zapíše číselnou hodnotu.
     *
     * @param value Hodnota.
     */
    public void writeRawLong(long value) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(value);
      write(bytes);
    }

    /**
     * Zapíše textovou hodnotu.
     *
     * @param text    Text.
     * @param charset Znaková sada.
     */
    public void writeRawString(String text, Charset charset) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(text, charset);
      write(bytes);
    }

    /**
     * Zapíše textovou hodnotu.
     *
     * @param text Text.
     */
    public void writeRawString(String text) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(text);
      write(bytes);
    }

    /**
     * Zapíše byte hodnotu.
     *
     * @param b Jeden byte.
     */
    public void writeRawByte(byte b) throws IOException {
      var bytes = IOBytesHelper.toRawBytes(b);
      write(bytes);
    }
  }
}
