package oc.mimic.jpack.helper;

import org.itadaki.bzip2.BZip2InputStream;
import org.itadaki.bzip2.BZip2OutputStream;
import org.tukaani.xz.LZMA2Options;
import org.tukaani.xz.XZInputStream;
import org.tukaani.xz.XZOutputStream;

import java.io.*;
import java.util.zip.*;

/**
 * Pomocné metody pro práci s komprimací dat.
 *
 * @author mimic
 */
public final class ZipHelper {

  /**
   * Komprese vstupních dat pomocí DEFLATE.
   *
   * @param bytes         Vstupní data.
   * @param compressLevel Úroveň komprese podle {@link Deflater#DEFAULT_COMPRESSION}.
   * @param bufferSize    Velikost bufferu.
   *
   * @return Komprimovaná data.
   */
  public static byte[] deflate(byte[] bytes, int compressLevel, int bufferSize) throws IOException {
    var deflater = new Deflater(compressLevel);
    byte[] deflated;

    try (var baos = new ByteArrayOutputStream()) {
      var buffer = new byte[bufferSize];
      deflater.setInput(bytes);
      deflater.finish();

      while (!deflater.finished()) {
        var read = deflater.deflate(buffer);
        baos.write(buffer, 0, read);
      }
      deflated = baos.toByteArray();
    } finally {
      deflater.end();
    }
    return deflated;
  }

  /**
   * Dekomprese vstupních dat pomocí DEFLATE.
   *
   * @param bytes      Vstupní data.
   * @param bufferSize Velikost bufferu.
   *
   * @return Dekomprimovaná data.
   */
  public static byte[] inflate(byte[] bytes, int bufferSize) throws IOException, DataFormatException {
    var inflater = new Inflater();
    byte[] result;

    try (var baos = new ByteArrayOutputStream()) {
      var buffer = new byte[bufferSize];
      inflater.setInput(bytes);

      while (!inflater.finished()) {
        var read = inflater.inflate(buffer);
        baos.write(buffer, 0, read);
      }
      result = baos.toByteArray();
    } finally {
      inflater.end();
    }
    return result;
  }

  /**
   * Komprese vstupních dat pomocí GZIP.
   *
   * @param bytes         Vstupní data.
   * @param compressLevel Úroveň komprese podle {@link Deflater#DEFAULT_COMPRESSION}.
   *
   * @return Komprimovaná data.
   */
  public static byte[] gzipCompress(byte[] bytes, int compressLevel) throws IOException {
    try (var baos = new ByteArrayOutputStream(bytes.length)) {
      try (var gzip = new CustomGZIPOutputStream(baos, compressLevel)) {
        gzip.write(bytes);
      }
      return baos.toByteArray();
    }
  }

  /**
   * Dekomprese vstupních dat pomocí GZIP.
   *
   * @param bytes      Vstupní data.
   * @param bufferSize Velikost bufferu.
   *
   * @return Dekomprimovaná data.
   */
  public static byte[] gzipUncompress(byte[] bytes, int bufferSize) throws IOException {
    byte[] result;

    try (var bais = new ByteArrayInputStream(bytes)) {
      try (var baos = new ByteArrayOutputStream()) {
        try (var gzip = new GZIPInputStream(bais)) {
          var buffer = new byte[bufferSize];
          int read;

          while ((read = gzip.read(buffer)) != -1) {
            baos.write(buffer, 0, read);
          }
        }
        result = baos.toByteArray();
      }
    }
    return result;
  }

  /**
   * Komprese vstupních dat pomocí BZIP2.
   *
   * @param bytes Vstupní data.
   *
   * @return Komprimovaná data.
   */
  public static byte[] bzip2Compress(byte[] bytes) throws IOException {
    try (var baos = new ByteArrayOutputStream(bytes.length)) {
      try (var bos = new BufferedOutputStream(baos)) {
        try (var bzip2 = new BZip2OutputStream(bos)) {
          bzip2.write(bytes);
        }
      }
      return baos.toByteArray();
    }
  }

  /**
   * Dekomprese vstupních dat pomocí BZIP2.
   *
   * @param bytes      Vstupní data.
   * @param bufferSize Velikost bufferu.
   *
   * @return Dekomprimovaná data.
   */
  public static byte[] bzip2Uncompress(byte[] bytes, int bufferSize) throws IOException {
    byte[] result;

    try (var bais = new ByteArrayInputStream(bytes)) {
      try (var baos = new ByteArrayOutputStream()) {
        try (var bzip2 = new BZip2InputStream(bais, false)) {
          var buffer = new byte[bufferSize];
          int read;

          while ((read = bzip2.read(buffer)) != -1) {
            baos.write(buffer, 0, read);
          }
        }
        result = baos.toByteArray();
      }
    }
    return result;
  }

  /**
   * Komprese vstupních dat pomocí XZ (obdoba LZMA2).
   *
   * @param bytes         Vstupní data.
   * @param compressLevel Úroveň komprese podle {@link LZMA2Options#PRESET_DEFAULT}.
   *
   * @return Komprimovaná data.
   */
  public static byte[] xzCompress(byte[] bytes, int compressLevel) throws IOException {
    try (var baos = new ByteArrayOutputStream(bytes.length)) {
      try (var bos = new BufferedOutputStream(baos)) {
        var options = new LZMA2Options();
        options.setPreset(compressLevel);

        try (var xz = new XZOutputStream(bos, options)) {
          xz.write(bytes);
        }
      }
      return baos.toByteArray();
    }
  }

  /**
   * Dekomprese vstupních dat pomocí XZ (obdoba LZMA2).
   *
   * @param bytes      Vstupní data.
   * @param bufferSize Velikost bufferu.
   *
   * @return Dekomprimovaná data.
   */
  public static byte[] xzUncompress(byte[] bytes, int bufferSize) throws IOException {
    byte[] result;

    try (var bais = new ByteArrayInputStream(bytes)) {
      try (var baos = new ByteArrayOutputStream()) {
        try (var xz = new XZInputStream(bais)) {
          var buffer = new byte[bufferSize];
          int read;

          while ((read = xz.read(buffer)) != -1) {
            baos.write(buffer, 0, read);
          }
        }
        result = baos.toByteArray();
      }
    }
    return result;
  }

  /**
   * Vlastní implementace GZIP výstupního streamu, který podporuje nastavení vlastní úrovně komprese.
   */
  private static final class CustomGZIPOutputStream extends GZIPOutputStream {

    /**
     * Vytvoří novou instanci streamu.
     *
     * @param out           Výstupní stream.
     * @param compressLevel Úroveň komprese podle {@link Deflater#DEFAULT_COMPRESSION}.
     */
    public CustomGZIPOutputStream(OutputStream out, int compressLevel) throws IOException {
      super(out);
      def.setLevel(compressLevel);
    }
  }
}
