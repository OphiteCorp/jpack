package oc.mimic.jpack.helper;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Pomocná třída pro náhodné rozházení pole podle seedu s možností zpětného sestavení.<br>
 * Jedná se o tzv. Fisher–Yates algoritmus.
 *
 * @author mimic
 */
public final class ShuffleHelper {

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(byte[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, secret, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(byte[] input, int[] exchanges) {
    shuffle(input, null, exchanges);
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param secret    Tajný kód. Může být null. Pokud nebude null, tak bude použit navíc XOR podle tajného klíče.
   * @param exchanges Náhodné indexy. Musí být vygenerovány podle tajného kódu.
   */
  public static void shuffle(byte[] input, byte[] secret, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];

        if (secret != null) {
          input[i] = xor(input[n], secret);
          input[n] = xor(tmp, secret);
        } else {
          input[i] = input[n];
          input[n] = tmp;
        }
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(byte[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, secret, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(byte[] input, int[] exchanges) {
    unshuffle(input, null, exchanges);
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param secret    Tajný kód. Může být null.
   * @param exchanges Náhodné indexy. Musí být vygenerovány podle tajného kódu.
   */
  public static void unshuffle(byte[] input, byte[] secret, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];

        if (secret != null) {
          input[i] = xor(input[n], secret);
          input[n] = xor(tmp, secret);
        } else {
          input[i] = input[n];
          input[n] = tmp;
        }
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(short[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(short[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(short[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(short[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(int[] input, long secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(int[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(int[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(int[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(int[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(float[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(float[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(float[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(float[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(double[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(double[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(double[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(double[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(long[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(long[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(long[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(long[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(char[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(char[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(char[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(char[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní pole podle tajného kódu.
   *
   * @param input  Vstupní pole.
   * @param secret Tajný kód.
   */
  public static void shuffle(boolean[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      shuffle(input, exchanges);
    }
  }

  /**
   * Promíchá vstupní pole podle výměnných indexů.
   *
   * @param input     Vstupní pole.
   * @param exchanges Náhodné indexy.
   */
  public static void shuffle(boolean[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = input.length - 1; i > 0; i--) {
        var n = exchanges[input.length - 1 - i];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input  Vstupní zamíchané pole.
   * @param secret Tajný kód.
   */
  public static void unshuffle(boolean[] input, byte[] secret) {
    if (input != null) {
      var exchanges = getShuffleExchanges(input.length, secret);
      unshuffle(input, exchanges);
    }
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí prvků v poli.
   *
   * @param input     Vstupní zamíchané pole.
   * @param exchanges Náhodné indexy.
   */
  public static void unshuffle(boolean[] input, int[] exchanges) {
    if (input != null) {
      if (exchanges == null) {
        throw new IllegalArgumentException("Input random exchange indexes are missing.");
      }
      for (var i = 1; i < input.length; i++) {
        var n = exchanges[input.length - i - 1];
        var tmp = input[i];
        input[i] = input[n];
        input[n] = tmp;
      }
    }
  }

  /**
   * Promíchá vstupní text podle tajného kódu.
   *
   * @param input  Vstupní text.
   * @param secret Tajný kód.
   *
   * @return Upravený vstupní text.
   */
  public static String shuffle(String input, byte[] secret) {
    if (input == null) {
      return null;
    }
    var chars = input.toCharArray();
    shuffle(chars, secret);
    return new String(chars);
  }

  /**
   * Promíchá vstupní text podle výměnných indexů.
   *
   * @param input     Vstupní text.
   * @param exchanges Náhodné indexy.
   *
   * @return Upravený vstupní text.
   */
  public static String shuffle(String input, int[] exchanges) {
    if (input == null) {
      return null;
    }
    var chars = input.toCharArray();
    shuffle(chars, exchanges);
    return new String(chars);
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí znaků v textu.
   *
   * @param input  Vstupní text.
   * @param secret Tajný kód.
   *
   * @return Upravený vstupní text.
   */
  public static String unshuffle(String input, byte[] secret) {
    if (input == null) {
      return null;
    }
    var chars = input.toCharArray();
    unshuffle(chars, secret);
    return new String(chars);
  }

  /**
   * Zvrátí proces zamíchání a obnoví pořadí znaků v textu.
   *
   * @param input     Vstupní text.
   * @param exchanges Náhodné indexy.
   *
   * @return Upravený vstupní text.
   */
  public static String unshuffle(String input, int[] exchanges) {
    if (input == null) {
      return null;
    }
    var chars = input.toCharArray();
    unshuffle(chars, exchanges);
    return new String(chars);
  }

  /**
   * Vygeneruje nový náhodný indexy podle tajného kódu.
   *
   * @param size   Velikost pole (počet indexů).
   * @param secret Tajný kód.
   *
   * @return Pole s náhodnými indexy.
   */
  public static int[] getShuffleExchanges(int size, byte[] secret) {
    if (size < 0) {
      throw new IllegalArgumentException("Size must not be less than 0. Size is: " + size);
    }
    if (secret == null) {
      throw new IllegalArgumentException("A secret code is required.");
    }
    var random = createSecureRandom();
    random.setSeed(secret);
    return getShuffleExchanges(size, random);
  }

  /**
   * Vygeneruje nový náhodný indexy podle tajného kódu.
   *
   * @param size   Velikost pole (počet indexů).
   * @param secret Tajný kód.
   *
   * @return Pole s náhodnými indexy.
   */
  public static int[] getShuffleExchanges(int size, long secret) {
    if (size < 0) {
      throw new IllegalArgumentException("Size must not be less than 0. Size is: " + size);
    }
    var random = createSecureRandom();
    random.setSeed(secret);
    return getShuffleExchanges(size, random);
  }

  /**
   * Vygeneruje nový náhodný indexy podle random generátoru.
   *
   * @param size   Velikost pole (počet indexů).
   * @param random Vlastní instance generátoru.
   *
   * @return Pole s náhodnými indexy.
   */
  public static int[] getShuffleExchanges(int size, Random random) {
    if (size < 0) {
      throw new IllegalArgumentException("Size must not be less than 0. Size is: " + size);
    }
    if (random == null) {
      throw new IllegalArgumentException("Missing random number generator instance.");
    }
    var exchanges = new int[size - 1];

    for (var i = size - 1; i > 0; i--) {
      var index = random.nextInt(i + 1);
      exchanges[size - 1 - i] = index;
    }
    return exchanges;
  }

  /**
   * Upraví byte podle tajného kódu.
   *
   * @param b      Vstupní byte.
   * @param secret Tajný kód.
   *
   * @return Upravený byte.
   */
  private static byte xor(byte b, byte[] secret) {
    if (secret != null) {
      for (var i = secret.length - 1; i >= 0; i--) {
        b ^= (secret[i] & 0xff);
      }
    }
    return b;
  }

  /**
   * Vytvoří novou instanci secure random generátoru podle SHA1PRNG.
   *
   * @param secret Tajný kód.
   *
   * @return Instance generátoru.
   */
  public static SecureRandom createSecureRandom(long secret) {
    var random = createSecureRandom();
    random.setSeed(secret);
    return random;
  }

  /**
   * Vytvoří novou instanci secure random generátoru podle SHA1PRNG.
   *
   * @param secret Tajný kód.
   *
   * @return Instance generátoru.
   */
  public static SecureRandom createSecureRandom(byte[] secret) {
    var random = createSecureRandom();
    random.setSeed(secret);
    return random;
  }

  /**
   * Vytvoří novou instanci secure random generátoru podle SHA1PRNG.
   *
   * @return Instance generátoru.
   */
  private static SecureRandom createSecureRandom() {
    try {
      return SecureRandom.getInstance("SHA1PRNG");
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("Invalid algorithm: SHA1PRNG", e);
    }
  }
}
